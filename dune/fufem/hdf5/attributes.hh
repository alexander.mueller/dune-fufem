#ifndef DUNE_FUFEM_HDF5_ATTRIBUTES_HH
#define DUNE_FUFEM_HDF5_ATTRIBUTES_HH

#include <string>

#include <hdf5.h>

#include "file.hh"

namespace HDF5 {
class Attribute {
public:
  Attribute(Grouplike &grouplike, std::string key)
      : grouplike_(grouplike), key_(key) {}

  bool exists() const { return H5Aexists(grouplike_.c_obj(), key_.c_str()); }

  std::string get() const {
    hid_t root = H5Gopen(grouplike_.c_obj(), "/", H5P_DEFAULT);
    hid_t attr = H5Aopen_name(root, key_.c_str());

    hid_t attrtype = H5Aget_type(attr);
    if (H5Tget_class(attrtype) != H5T_STRING)
      DUNE_THROW(Dune::Exception,
                 "Only string attributes are supported");

    if (H5Tis_variable_str(attrtype))
      DUNE_THROW(Dune::Exception,
                 "Variable-size attributes are not supported");

    hid_t nativetype = H5Tget_native_type(attrtype, H5T_DIR_ASCEND);
    std::vector<char> buffer(H5Tget_size(attrtype));
    if (H5Aread(attr, nativetype, buffer.data()) < 0)
      DUNE_THROW(Dune::Exception, "Failed to read attribute: " << key_);
    H5Tclose(nativetype);

    H5Aclose(attr);
    H5Gclose(root);

    return {buffer.begin(), buffer.end()};
  }

  void set(std::string value) {
    hid_t stringtype = H5Tcopy(H5T_C_S1); // A C-style string
    H5Tset_size(stringtype, value.size());
    hid_t dataspace = H5Screate(H5S_SCALAR);

    hid_t root = H5Gopen(grouplike_.c_obj(), "/", H5P_DEFAULT);
    hid_t attr = H5Acreate(root, key_.c_str(), stringtype, dataspace,
                           H5P_DEFAULT, H5P_DEFAULT);
    H5Awrite(attr, stringtype, value.c_str());

    H5Aclose(attr);
    H5Gclose(root);
    H5Sclose(dataspace);
    H5Tclose(stringtype);
  }

private:
  Grouplike &grouplike_;
  std::string key_;
};
}
#endif
