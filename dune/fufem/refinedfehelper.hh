// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_FUFEM_REFINED_FE_HELPER_HH
#define DUNE_FUFEM_REFINED_FE_HELPER_HH

#include <dune/localfunctions/common/virtualinterface.hh>
#include <dune/localfunctions/common/virtualwrappers.hh>

#include <dune/localfunctions/refined/refinedp0.hh>
#include <dune/localfunctions/refined/refinedp1.hh>

/** \brief Method to detect if a finite element is a refined finite element. */
template <class LocalFiniteElementType>
struct IsRefinedLocalFiniteElement
{
    static bool value(const LocalFiniteElementType&)
    {
        return false;
    }
};

template <class T>
struct IsRefinedLocalFiniteElement<typename Dune::template LocalFiniteElementVirtualInterface<T> >
{
    using LocalFiniteElementType = Dune::LocalFiniteElementVirtualInterface<T>;

    static bool value(const LocalFiniteElementType& fe)
    {
        using Traits = typename LocalFiniteElementType::Traits::LocalBasisType::Traits;
        using DT = typename Traits::DomainFieldType;
        using RT = typename Traits::RangeFieldType;
        static constexpr int dim = Traits::dimDomain;

        using RP1FE = Dune::RefinedP1LocalFiniteElement<DT, RT, dim>;
        using RP0FE = Dune::RefinedP0LocalFiniteElement<DT, RT, dim>;

        if (dynamic_cast<const Dune::LocalFiniteElementVirtualImp<RP1FE>*>(&fe))
            return true;
        if (dynamic_cast<const Dune::LocalFiniteElementVirtualImp<RP0FE>*>(&fe))
            return true;
        return false;
    }
};

template <class ctype, class RT, int dim>
struct IsRefinedLocalFiniteElement<Dune::RefinedP1LocalFiniteElement<ctype, RT, dim> >
{
    static bool value(const Dune::RefinedP1LocalFiniteElement<ctype, RT, dim>&)
    {
        return true;
    }
};

template <class ctype, class RT, int dim>
struct IsRefinedLocalFiniteElement<Dune::RefinedP0LocalFiniteElement<ctype, RT, dim> >
{
    static bool value(const Dune::RefinedP0LocalFiniteElement<ctype, RT, dim>&)
    {
        return true;
    }
};



#endif

