#ifndef ISOTROPIC_TENSOR_HH
#define ISOTROPIC_TENSOR_HH

#include "elasticitytensor.hh"

template <int dim>
class IsotropicTensor : public ElasticityTensor<dim>
{
public:
  IsotropicTensor(double E, double nu)
  {
    DUNE_THROW(Dune::NotImplemented, "No isotropic Hooke tensor for " << dim << "-dimensional problems");
  }
};

template <>
class IsotropicTensor<3> : public ElasticityTensor<3>
{
public:
  /* Same representation as in symmetrictensor.hh:

    [s11]   [ * * *       ]   [e11]
    [s22]   [ * * *       ]   [e22]
    [s33] = [ * * *       ] * [e33]
    [s12]   [       *     ]   [e12]
    [s13]   [         *   ]   [e13]
    [s23]   [           * ]   [e23]
  */
  IsotropicTensor(double E, double nu)
  {
    ElasticityTensor<3>::operator=(0.0);

    (*this)[0][0] = 1 - nu;
    (*this)[0][1] = nu;
    (*this)[0][2] = nu;

    (*this)[1][0] = nu;
    (*this)[1][1] = 1 - nu;
    (*this)[1][2] = nu;

    (*this)[2][0] = nu;
    (*this)[2][1] = nu;
    (*this)[2][2] = 1 - nu;

    (*this)[3][3] = 1 - 2*nu;
    (*this)[4][4] = 1 - 2*nu;
    (*this)[5][5] = 1 - 2*nu;

    (*this) *= E/(1 + nu)/(1 - 2*nu);
  }
};

template <>
class IsotropicTensor<2> : public ElasticityTensor<2>
{
public:
  IsotropicTensor(double E, double nu)
  {
    ElasticityTensor<2>::operator=(0.0);

    (*this)[0][0] = 1 - nu;
    (*this)[0][1] = nu;

    (*this)[1][0] = nu;
    (*this)[1][1] = 1 - nu;

    (*this)[2][2] = 1 - 2*nu;

    (*this) *= (E/(1 + nu)/(1 - 2*nu));
  }
};

#endif
