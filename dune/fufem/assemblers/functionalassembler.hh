#ifndef FUNCTIONAL_ASSEMBLER_HH
#define FUNCTIONAL_ASSEMBLER_HH

#include <dune/istl/bvector.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"

//! Generic global assembler for functionals on a gridview
template <class TestBasis>
class FunctionalAssembler
{
    private:
        typedef typename TestBasis::GridView GridView;

    public:
        //! create assembler for gridview
        FunctionalAssembler(const TestBasis& tBasis) :
            tBasis_(tBasis)
        {}

        /** \brief Assemble
         *
         * \param localAssembler local assembler
         * \param[out] b target vector
         * \param initializeVector If this is set the output vector is
         * set to the correct length and initialized to zero before
         * assembly. Otherwise the assembled values are just added to
         * the vector.
         */
        template <class LocalFunctionalAssemblerType, class GlobalVectorType>
        void assemble(LocalFunctionalAssemblerType& localAssembler, GlobalVectorType& b, bool initializeVector=true) const
        {
            typedef typename LocalFunctionalAssemblerType::LocalVector LocalVector;
            typedef typename TestBasis::LinearCombination LinearCombination;

            int rows = tBasis_.size();

            if (initializeVector)
            {
                b.resize(rows);
                b=0.0;
            }

            for (const auto& element : elements(tBasis_.getGridView()))
            {
                // get shape functions
                const typename TestBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(element);

                LocalVector localB(tFE.localBasis().size());
                localAssembler.assemble(element, localB, tFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int idx = tBasis_.index(element, i);
                    const LinearCombination& constraints = tBasis_.constraints(idx);
                    bool isConstrained = tBasis_.isConstrained(idx);
                    if (isConstrained)
                    {
                        for (size_t w=0; w<constraints.size(); ++w)
                            b[constraints[w].index].axpy(constraints[w].factor, localB[i]);
                    }
                    else
                        b[idx] += localB[i];
                }
            }
            return;
        }

    private:
        const TestBasis& tBasis_;
};

#endif

