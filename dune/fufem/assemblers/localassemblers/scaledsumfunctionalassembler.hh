#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SCALEDSUMFUNCTIONALASSEMBLER_HH
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SCALEDSUMFUNCTIONALASSEMBLER_HH

#include <dune/matrix-vector/axpy.hh>

#include <dune/fufem/assemblers/localfunctionalassembler.hh>

template <typename Grid, typename FE, typename T>
class ScaledSumFunctionalAssembler
    : public LocalFunctionalAssembler<Grid, FE, T> {
public:
  using Base = LocalFunctionalAssembler<Grid, FE, T>;
  using Assembler = Base;
  using typename Base::Element;
  using typename Base::LocalVector;

  void registerAssembler(double scale, const Assembler& assembler) {
    if (scale == 0.0)
      return;
    scaledAssemblers_.emplace_back(scale, &assembler);
  }

  template <class AssemblerType>
  void registerAssembler(double scale, AssemblerType assembler) {
    if (scale == 0.0)
      return;
    heldAssemblers_.emplace_back(
        std::make_shared<AssemblerType>(std::move(assembler)));
    scaledAssemblers_.emplace_back(scale, heldAssemblers_.back().get());
  }

  template <class AssemblerType, class... Args>
  void registerAssembler(double scale, Args&&... args) {
    if (scale == 0.0)
      return;
    heldAssemblers_.emplace_back(
        std::make_shared<AssemblerType>(std::forward<Args>(args)...));
    scaledAssemblers_.emplace_back(scale, heldAssemblers_.back().get());
  }

  /**
   * \brief assemble the sum of all previously registered assemblers scaled by
   *        their respective factors
   */
  virtual void assemble(const Element& element, LocalVector& localVector,
                        const FE& tFE) const {
    localVector = 0.0;
    for (auto&& scaledAssembler : scaledAssemblers_) {
      LocalVector v(localVector.size());
      scaledAssembler.second->assemble(element, v = 0.0, tFE);
      for (size_t i = 0; i < v.size(); ++i)
        Dune::MatrixVector::addProduct(localVector[i], scaledAssembler.first,
                                       v[i]);
    }
  }

private:
  std::vector<std::shared_ptr<Assembler>> heldAssemblers_;
  std::vector<std::pair<double, const Assembler*>> scaledAssemblers_;
};

#endif // DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_SCALEDSUMFUNCTIONALASSEMBLER_HH
