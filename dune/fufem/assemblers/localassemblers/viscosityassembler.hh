#ifndef VISCOSITY_ASSEMBLER_HH
#define VISCOSITY_ASSEMBLER_HH

#include <memory>

#include "dune/fufem/assemblers/localassemblers/secondorderoperatorassembler.hh"
#include "dune/fufem/assemblers/localassemblers/strainproductassembler.hh"
#include "dune/fufem/symmetrictensor.hh"
#include <dune/fufem/functions/gridfunctionhelper.hh>

#include <dune/functions/gridfunctions/gridviewfunction.hh>

template <class GradientVector, class LocalMatrix>
inline LocalMatrix isotropicNewtonianViscosityContraction(
    const GradientVector& g1, const GradientVector& g2,
    typename GradientVector::field_type muShear,
    typename GradientVector::field_type muBulk) {
    using ctype = typename GradientVector::field_type;
    int const dim = GradientVector::dimension;
    LocalMatrix R = Dune::ScaledIdentityMatrix<ctype,dim>(muShear*(g1*g2));
    for(std::size_t i=0; i<R.N(); ++i)
      for(std::size_t j=0; j<R.N(); ++j)
        R[i][j] += muShear * (g1[j]*g2[i])
          + (muBulk-(2.0/3.0)*muShear) * (g1[i]*g2[j]);
    return R;
}

template <class Grid, class LFE, class LocalMatrix>
auto getIsotropicNewtonianViscosityAssembler(Grid const& grid, double muShear,
                                             double muBulk) {
  using GlobalCoordinate =
      typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
  // decltype(contraction) needs to be a lambda rather than a function pointer
  // in order to allow for it to be inlined
  auto const contraction = [](auto&&... args) {
    return isotropicNewtonianViscosityContraction<GlobalCoordinate,
                                                  LocalMatrix>(
        std::forward<decltype(args)>(args)...);
  };
  size_t const dim = GlobalCoordinate::dimension;
  auto const leafView = grid.leafGridView();

  auto funcMuShear = [muShear](GlobalCoordinate const &) {return muShear;};
  auto funcMuBulk = [muBulk](GlobalCoordinate const &) {return muBulk;};
  auto gridFuncMuShear = Dune::Functions::makeGridViewFunction(funcMuShear, leafView);
  auto gridFuncMuBulk = Dune::Functions::makeGridViewFunction(funcMuBulk, leafView);

  return SecondOrderOperatorAssembler<Grid, LFE, LFE, decltype(contraction),
                                      LocalMatrix, decltype(gridFuncMuShear),
                                      decltype(gridFuncMuBulk)>(
      contraction, true, QuadratureRuleKey(dim, 0), gridFuncMuShear,
      gridFuncMuBulk);
}

template <class Grid, class LFE, class LocalMatrix, class F1, class F2>
auto getIsotropicNewtonianViscosityAssembler(
    Grid const& grid, const F1& muShear, const F2& muBulk,
    QuadratureRuleKey quadratureRuleKey) {
  using GlobalCoordinate = typename Grid::template Codim<0>::Geometry::GlobalCoordinate;
  // decltype(contraction) needs to be a lambda rather than a function pointer
  // in order to allow for it to be inlined
  auto const contraction = [](auto&&... args) {
    return isotropicNewtonianViscosityContraction<GlobalCoordinate,
                                                  LocalMatrix>(
        std::forward<decltype(args)>(args)...);
  };
  auto const leafView = grid.leafGridView();

  auto gridFuncMuShear = Dune::Functions::makeGridViewFunction(muShear, leafView);
  auto gridFuncMuBulk = Dune::Functions::makeGridViewFunction(muBulk, leafView);

  return SecondOrderOperatorAssembler<Grid, LFE, LFE, decltype(contraction),
                                      LocalMatrix, decltype(gridFuncMuShear),
                                      decltype(gridFuncMuBulk)>(
      contraction, true, quadratureRuleKey, gridFuncMuShear, gridFuncMuBulk);
}

/** \brief Local assembler for the viscous part of the linear visco-elastic Kelvin-Voigt material. */
template <class GridType, class TestLocalFE, class AnsatzLocalFE>
class ViscosityAssembler
    : public StrainProductAssembler< GridType, TestLocalFE, AnsatzLocalFE, ViscosityAssembler<GridType,TestLocalFE,AnsatzLocalFE> >
{
    private:
        typedef StrainProductAssembler< GridType, TestLocalFE, AnsatzLocalFE, ViscosityAssembler<GridType,TestLocalFE,AnsatzLocalFE> > SPA;
        using SPA::dim;
        using typename SPA::ctype;

        /** \brief shear viscosity */
        const ctype muShear_;

        /**  \brief bulk viscosity */
        const ctype muBulk_;

    public:
        using typename SPA::T;
        using typename SPA::BoolMatrix;
        using typename SPA::Element;
        using typename SPA::LocalMatrix;

    ViscosityAssembler(ctype muShear, ctype muBulk):
        muShear_(muShear), muBulk_(muBulk)
    {}

    SymmetricTensor<dim> strainToStress(const SymmetricTensor<dim>& strain,
                                        const Element&,
                                        const Dune::FieldVector<ctype, dim>&) const {
            SymmetricTensor<dim> stress = strain;
            stress *= 2*muShear_;
            stress.addToDiag((muBulk_-(2.0/3.0)*muShear_) * strain.trace());
            return stress;
    }


};

/** \brief Local assembler for the viscous part of the linear visco-elastic Kelvin-Voigt material. With variable coefficients. */
template <class GridType, class TestLocalFE, class AnsatzLocalFE, class FunctionType>
class VariableCoefficientViscosityAssembler
    : public StrainProductAssembler< GridType, TestLocalFE, AnsatzLocalFE, VariableCoefficientViscosityAssembler<GridType,TestLocalFE,AnsatzLocalFE,FunctionType> >
{
    private:
        using Self = VariableCoefficientViscosityAssembler<GridType,TestLocalFE,AnsatzLocalFE, FunctionType>;
        using SPA = StrainProductAssembler< GridType, TestLocalFE, AnsatzLocalFE, Self >;
        using SPA::dim;
        using typename SPA::ctype;

        using GlobalCoordinate = typename GridType::template Codim<0>::Geometry::GlobalCoordinate;
        using LocalCoordinate = typename GridType::template Codim<0>::Geometry::LocalCoordinate;
        using RangeType = std::decay_t<std::invoke_result_t<FunctionType,GlobalCoordinate>>;
        using LocalFunction = Dune::Fufem::Impl::LocalFunctionInterfaceForGrid<GridType, RangeType>;

    public:
        using typename SPA::T;
        using typename SPA::BoolMatrix;
        using typename SPA::Element;
        using typename SPA::LocalMatrix;

    template<class FT1, class FT2>
    VariableCoefficientViscosityAssembler(const GridType& grid,
                                          const FT1& muShear,
                                          const FT2& muBulk,
                                          const QuadratureRuleKey& muShearKey,
                                          const QuadratureRuleKey& muBulkKey)
      : SPA(muShearKey.sum(muBulkKey)),
        localMuShear_(Dune::Fufem::Impl::localFunctionForGrid<GridType, RangeType>(muShear)),
        localMuBulk_(Dune::Fufem::Impl::localFunctionForGrid<GridType, RangeType>(muBulk))
    {}

    SymmetricTensor<dim> strainToStress(const SymmetricTensor<dim>& strain,
                                        const Element& element,
                                        const Dune::FieldVector<ctype, dim>& position) const {
            localMuShear_.bind(element);
            localMuBulk_.bind(element);
            RangeType muShear = localMuShear_(position);
            RangeType muBulk = localMuBulk_(position);

            SymmetricTensor<dim> stress = strain;
            stress *= 2*muShear;
            stress.addToDiag((muBulk-(2.0/3.0)*muShear) * strain.trace());
            return stress;
    }
    protected:
        mutable LocalFunction localMuShear_;
        mutable LocalFunction localMuBulk_;
};
#endif
