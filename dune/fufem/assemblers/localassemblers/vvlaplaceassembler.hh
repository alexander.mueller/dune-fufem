#ifndef DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_VVLAPLACEASSEMBLER
#define DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_VVLAPLACEASSEMBLER

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localoperatorassembler.hh>

/** \brief Local assembler for the Laplace problem */
template <class GridType, class TestLocalFE, class AnsatzLocalFE, class T=Dune::FieldMatrix<double,1,1> >
class VVLaplaceAssembler : public LocalOperatorAssembler <GridType, TestLocalFE, AnsatzLocalFE, T >
{
  private:
    typedef LocalOperatorAssembler < GridType, TestLocalFE, AnsatzLocalFE ,T > Base;
    static const int dim      = GridType::dimension;
    static const int dimworld = GridType::dimensionworld;

  public:
    typedef typename Base::Element Element;
    typedef typename Element::Geometry Geometry;
    typedef typename Geometry::JacobianInverseTransposed JacobianInverseTransposed;
    typedef typename Base::BoolMatrix BoolMatrix;
    typedef typename Base::LocalMatrix LocalMatrix;

  private:
    const T& one_;

  public:
    VVLaplaceAssembler(const T& one)
      : one_(one)
    {}

    void indices([[maybe_unused]] const Element& element, BoolMatrix& isNonZero, [[maybe_unused]] const TestLocalFE& tFE, [[maybe_unused]] const AnsatzLocalFE& aFE) const
    {
      isNonZero = true;
    }

    template <class BoundaryIterator>
    void indices([[maybe_unused]] const BoundaryIterator& it, BoolMatrix& isNonZero, [[maybe_unused]] const TestLocalFE& tFE, [[maybe_unused]] const AnsatzLocalFE& aFE) const
    {
      isNonZero = true;
    }

    /** \brief Assemble the local stiffness matrix for a given element
         */
    void assemble(const Element& element, LocalMatrix& localMatrix, const TestLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
      typedef typename Dune::template FieldVector<double,dim> FVdim;
      typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;
      typedef typename TestLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

      // Make sure we got suitable shape functions
      assert(tFE.type() == element.type());
      assert(aFE.type() == element.type());

      // check if ansatz local fe = test local fe
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

      int rows = localMatrix.N();
      int cols = localMatrix.M();

      // get geometry and store it
      const Geometry geometry = element.geometry();

      localMatrix = 0.0;

      // get quadrature rule
      QuadratureRuleKey tFEquad(tFE);
      QuadratureRuleKey quadKey = tFEquad.derivative().square();
      const Dune::template QuadratureRule<double, dim>& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

      // store gradients of shape functions and base functions
      std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
      std::vector<FVdimworld> gradients(tFE.localBasis().size());


      // loop over quadrature points
      for (size_t pt=0; pt < quad.size(); ++pt)
      {
        // get quadrature point
        const FVdim& quadPos = quad[pt].position();

        // get transposed inverse of Jacobian of transformation
        const JacobianInverseTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

        // get integration factor
        const double integrationElement = geometry.integrationElement(quadPos);

        // get gradients of shape functions
        tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

        // transform gradients
        for (size_t i=0; i<gradients.size(); ++i)
          invJacobian.mv(referenceGradients[i][0], gradients[i]);

        // compute matrix entries
        double z = quad[pt].weight() * integrationElement;
        for (int i=0; i<rows; ++i)
        {
          for (int j=i+1; j<cols; ++j)
          {
            double zij = (gradients[i] * gradients[j]) * z;
            Dune::MatrixVector::addProduct(localMatrix[i][j], zij, one_);
            Dune::MatrixVector::addProduct(localMatrix[j][i], zij, one_);
          }
          Dune::MatrixVector::addProduct(localMatrix[i][i], gradients[i] * gradients[i] * z, one_);
        }
      }
      return;
    }

    /** \brief Assemble the local stiffness matrix for a given boundary face
         */
    template <class BoundaryIterator>
    void assemble(const BoundaryIterator& it, LocalMatrix& localMatrix, const TestLocalFE& tFE, const AnsatzLocalFE& aFE) const
    {
      typedef typename Dune::template FieldVector<double,dimworld> FVdimworld;

      typedef typename BoundaryIterator::Intersection::Geometry Geometry;
      typedef typename BoundaryIterator::Intersection::Entity::Geometry InsideGeometry;
      typedef typename InsideGeometry::JacobianInverseTransposed JacobianInverseTransposed;
      typedef typename TestLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

      // Make sure we got suitable shape functions
      assert(tFE.type() == it->inside()->type());
      assert(aFE.type() == it->inside()->type());

      // check if ansatz local fe = test local fe
      if (not Base::isSameFE(tFE, aFE))
        DUNE_THROW(Dune::NotImplemented, "LaplaceAssembler is only implemented for ansatz space=test space!");

      int rows = localMatrix.N();
      int cols = localMatrix.M();

      // get geometry and store it
      const Geometry intersectionGeometry = it->geometry();
      const InsideGeometry insideGeometry = it->inside()->geometry();

      localMatrix = 0.0;

      // get quadrature rule
      QuadratureRuleKey tFEquad(it->type(), tFE.localBasis().order());
      QuadratureRuleKey quadKey = tFEquad.derivative().square();

      const Dune::template QuadratureRule<double, dim-1>& quad = QuadratureRuleCache<double, dim-1>::rule(quadKey);

      // store gradients of shape functions and base functions
      std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
      std::vector<FVdimworld> gradients(tFE.localBasis().size());

      // loop over quadrature points
      for (size_t pt=0; pt < quad.size(); ++pt)
      {
        // get quadrature point
        const Dune::FieldVector<double,dim-1>& quadPos = quad[pt].position();

        // get transposed inverse of Jacobian of transformation
        const JacobianInverseTransposed& invJacobian = insideGeometry.jacobianInverseTransposed(it->geometryInInside().global(quadPos));

        // get integration factor
        const double integrationElement = intersectionGeometry.integrationElement(quadPos);

        // get gradients of shape functions
        tFE.localBasis().evaluateJacobian(it->geometryInInside().global(quadPos), referenceGradients);

        // Project gradient on the inside element tangent plane, to obtain
        // the correct surface gradient
        // Step 1: get an orthonormal basis of the tangent space at the current quadrature point
        typename GridType::template Codim<1>::LocalGeometry::JacobianTransposed tangentSpaceBasis = it->geometryInInside().jacobianTransposed(quadPos);
        Dune::FieldMatrix<double,dim-1,dim> orthonormalBasis = makeOrthonormal(tangentSpaceBasis);

        // Step 2: project
        std::vector<Dune::FieldVector<double,dim> > projectedReferenceGradients(tFE.localBasis().size());
        for (size_t i=0; i<projectedReferenceGradients.size(); i++) {
          projectedReferenceGradients[i] = 0;
          for (size_t j=0; j<dim-1; j++)
            projectedReferenceGradients[i].axpy(referenceGradients[i][0]*orthonormalBasis[j],orthonormalBasis[j]);
        }

        // transform gradients
        for (size_t i=0; i<gradients.size(); ++i)
          invJacobian.mv(projectedReferenceGradients[i], gradients[i]);

        // compute matrix entries
        double z = quad[pt].weight() * integrationElement;
        for (int i=0; i<rows; ++i)
        {
          for (int j=i+1; j<cols; ++j)
          {
            double zij = (gradients[i] * gradients[j]) * z;
            Dune::MatrixVector::addProduct(localMatrix[i][j], zij, one_);
            Dune::MatrixVector::addProduct(localMatrix[j][i], zij, one_);
          }
          Dune::MatrixVector::addProduct(localMatrix[i][i], gradients[i] * gradients[i] * z, one_);
        }
      }

    }

};


#endif // DUNE_FUFEM_ASSEMBLERS_LOCALASSEMBLERS_VVLAPLACEASSEMBLER

