// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef SUBGRID_H1_FUNCTIONAL_ASSEMBLER_HH
#define SUBGRID_H1_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/functions/common/defaultderivativetraits.hh>

#include <dune/fufem/functions/gridfunctionhelper.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>
#include <dune/fufem/assemblers/localassemblers/h1functionalassembler.hh>

/**  \brief Local assembler for finite element H^1-functionals on the Subgrid, given by Hostgrid-functions
  *
  *  This is needed, e.g. when assembling the right hand side of the spatial problem of a time-discretized time dependent problem with spatial adaptivity.
  *  The solution of the old time step lives on the hostgrid while the rhs is assembled on the NEW subgrid.
  */
template <class GridType, class TestLocalFE, class T=Dune::FieldVector<double,1> >
class SubgridH1FunctionalAssembler :
    public H1FunctionalAssembler<GridType, TestLocalFE, T>

{
    private:
        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

        typedef typename GridType::HostGridType HostGrid;

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

        using FRangeType = typename Dune::Functions::DefaultDerivativeTraits<T(GlobalCoordinate)>::Range;
        using LocalHostFunction = Dune::Fufem::Impl::LocalFunctionInterfaceForGrid<HostGrid, FRangeType>;

    public:
        typedef typename LocalFunctionalAssembler<GridType,TestLocalFE,T>::Element Element;
        typedef typename Element::Geometry Geometry;
        typedef typename LocalFunctionalAssembler<GridType,TestLocalFE,T>::LocalVector LocalVector;

        typedef typename GridType::HostGridType::template Codim<0>::Entity::Geometry HostGeometry;

        /** \brief constructor
          *
          * Creates a local functional assembler for an H1-functional.
          * It can assemble functionals on the subgrid given by grid
          * functions on the underlying hostgrid exactly.
          *
          * The QuadratureRuleKey given here does specify what is
          * needed to integrate f.
          *
          * \param f the (hostgrid) function representing the functional (if assembling e.g. \f$(\nabla u,\nabla v)\f$ you need to provide \f$f=\nabla u\f$)
          * \param grid the subgrid (!)
          * \param fQuadKey A QuadratureRuleKey that specifies how to integrate f
          */
        template<class FT>
        SubgridH1FunctionalAssembler(const FT& f, const GridType& grid, const QuadratureRuleKey& fQuadKey) :
            H1FunctionalAssembler<GridType,TestLocalFE, T>(f, fQuadKey),
            grid_(grid),
            localHostF_(Dune::Fufem::Impl::localFunctionForGrid<HostGrid, FRangeType>(f)),
            functionQuadKey_(fQuadKey)
        {}

        /** \copydoc H1FunctionalAssembler::assemble
         */
        void assemble(const Element& element, LocalVector& localVector, const TestLocalFE& tFE) const
        {
            typedef typename TestLocalFE::Traits::LocalBasisType::Traits::JacobianType LocalBasisJacobianType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            const auto hostelement = grid_.template getHostEntity<0>(element);

            // store gradients of shape functions and base functions
            std::vector<LocalBasisJacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<GlobalCoordinate> gradients(tFE.localBasis().size());

            if (hostelement.isLeaf())
            {
                // get quadrature rule
                QuadratureRuleKey quadKey(tFE);
                quadKey.setGeometryType(element.type());
                quadKey = quadKey.derivative().product(functionQuadKey_);
                const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

                localHostF_.bind(hostelement);

                // loop over quadrature points
                for (size_t pt=0; pt < quad.size(); ++pt)
                {
                    // get quadrature point
                    const LocalCoordinate& quadPos = quad[pt].position();

                    // get transposed inverse of Jacobian of transformation
                    const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                    // get integration factor
                    const double integrationElement = geometry.integrationElement(quadPos);

                    // get gradients of shape functions
                    tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                    // transform gradients
                    for (size_t i=0; i<gradients.size(); ++i)
                        invJacobian.mv(referenceGradients[i][0], gradients[i]);

                    // compute values of function
                    auto f_pos = localHostF_(quadPos);

                    // and vector entries
                    for (size_t i=0; i<gradients.size(); ++i)
                    {
                        T dummy;
                        f_pos.mv(gradients[i],dummy);
                        localVector[i].axpy(quad[pt].weight()*integrationElement, dummy);
                    }
                }
            }
            else // corresponding hostgrid element is not in hostgrid leaf
            {
                for (const auto& descElement : descendantElements(hostelement, grid_.getHostGrid().maxLevel()))
                {
                    if (descElement.isLeaf())
                    {
                        const HostGeometry hostGeometry = descElement.geometry();

                        // get quadrature rule
                        QuadratureRuleKey quadKey(tFE);
                        quadKey.setGeometryType(descElement.type());
                        quadKey = quadKey.product(functionQuadKey_);
                        const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

                        localHostF_.bind(descElement);

                        // loop over quadrature points
                        for (size_t pt=0; pt < quad.size(); ++pt)
                        {
                            // get quadrature point
                            const LocalCoordinate& quadPos = quad[pt].position();
                            const LocalCoordinate quadPosInSubgridElement = geometry.local(hostGeometry.global(quadPos)) ;

                            // get integration factor
                            const double integrationElement = hostGeometry.integrationElement(quadPos);

                            // get transposed inverse of Jacobian of transformation
                            const auto& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                            // get gradients of shape functions
                            tFE.localBasis().evaluateJacobian(quadPosInSubgridElement, referenceGradients);

                            // transform gradients
                            for (size_t i=0; i<gradients.size(); ++i)
                                invJacobian.mv(referenceGradients[i][0], gradients[i]);

                            // compute values of function
                            auto f_pos = localHostF_(quadPos);

                            // and vector entries
                            for (size_t i=0; i<gradients.size(); ++i)
                            {
                                T dummy;
                                f_pos.mv(gradients[i],dummy);
                                localVector[i].axpy(quad[pt].weight()*integrationElement, dummy);
                            }
                        }
                    }
                }
            }

            return;
        }

    private:
        const GridType& grid_;
        mutable LocalHostFunction localHostF_;
        const QuadratureRuleKey functionQuadKey_;
};

#endif

