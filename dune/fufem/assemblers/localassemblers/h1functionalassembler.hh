// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef H1_FUNCTIONAL_ASSEMBLER_HH
#define H1_FUNCTIONAL_ASSEMBLER_HH


#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include <dune/functions/common/defaultderivativetraits.hh>

#include <dune/fufem/functions/gridfunctionhelper.hh>
#include <dune/fufem/quadraturerules/quadraturerulecache.hh>
#include <dune/fufem/assemblers/localfunctionalassembler.hh>

/**  \brief Local assembler for finite element H^1-functionals
*/
template <class GridType, class TestLocalFE, class T=Dune::FieldVector<double,1> >
class H1FunctionalAssembler :
    public LocalFunctionalAssembler<GridType, TestLocalFE, T>

{
    private:
        typedef LocalFunctionalAssembler<GridType, TestLocalFE, T> Base;
        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate GlobalCoordinate;
        typedef typename GridType::template Codim<0>::Geometry::LocalCoordinate LocalCoordinate;

        using FRangeType = typename Dune::Functions::DefaultDerivativeTraits<T(GlobalCoordinate)>::Range;
        using LocalFunction = Dune::Fufem::Impl::LocalFunctionInterfaceForGrid<GridType, FRangeType>;

        static const int dim = GridType::dimension;
        static const int dimworld = GridType::dimensionworld;

    public:
        typedef typename Base::Element Element;
        typedef typename Base::Element::Geometry Geometry;
        typedef typename Base::LocalVector LocalVector;

        /** \brief constructor
          *
          * \param f the function representing the functional (if assembling e.g. \f$(\nabla u,\nabla v)\f$ you need to provide \f$f=\nabla u\f$)
          * \param fQuadKey A QuadratureRuleKey that specifies how to integrate f
          */
        template<class FT>
        H1FunctionalAssembler(const FT& f, const QuadratureRuleKey& fQuadKey) :
            localF_(Dune::Fufem::Impl::localFunctionForGrid<GridType, FRangeType>(f)),
            functionQuadKey_(fQuadKey)
        {}

        /** \copydoc LocalFunctionalAssembler::assemble
         */
        virtual void assemble(const Element& element, LocalVector& localVector, const TestLocalFE& tFE) const
        {
            typedef typename GridType::template Codim<0>::Geometry::JacobianInverseTransposed GeoJacobianInvTransposed;
            typedef typename TestLocalFE::Traits::LocalBasisType::Traits::JacobianType JacobianType;

            // Make sure we got suitable shape functions
            assert(tFE.type() == element.type());

            // get geometry and store it
            const Geometry geometry = element.geometry();

            localVector = 0.0;

            // get quadrature rule
            QuadratureRuleKey tFEquad(tFE);
            QuadratureRuleKey quadKey = tFEquad.derivative().product(functionQuadKey_);
            const auto& quad = QuadratureRuleCache<double, dim>::rule(quadKey);

            // store gradients of shape functions and base functions
            std::vector<JacobianType> referenceGradients(tFE.localBasis().size());
            std::vector<GlobalCoordinate> gradients(tFE.localBasis().size());

            localF_.bind(element);

            // loop over quadrature points
            for (size_t pt=0; pt < quad.size(); ++pt)
            {
                // get quadrature point
                const LocalCoordinate& quadPos = quad[pt].position();

                // get transposed inverse of Jacobian of transformation
                const GeoJacobianInvTransposed& invJacobian = geometry.jacobianInverseTransposed(quadPos);

                // get integration factor
                const double integrationElement = geometry.integrationElement(quadPos);

                // get gradients of shape functions
                tFE.localBasis().evaluateJacobian(quadPos, referenceGradients);

                // transform gradients
                for (size_t i=0; i<gradients.size(); ++i)
                    invJacobian.mv(referenceGradients[i][0], gradients[i]);

                // compute values of function
                auto f_pos = localF_(quadPos);

                // and vector entries
                for (size_t i=0; i<gradients.size(); ++i)
                {
                    T dummy;
                    f_pos.mv(gradients[i],dummy);
                    localVector[i].axpy(quad[pt].weight()*integrationElement, dummy);
                }
            }
            return;
        }

    private:
        mutable LocalFunction localF_;
        const QuadratureRuleKey functionQuadKey_;
};

#endif

