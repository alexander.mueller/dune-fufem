#ifndef BOUNDARY_OPERATOR_ASSEMBLER_HH
#define BOUNDARY_OPERATOR_ASSEMBLER_HH

#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>

#include <dune/matrix-vector/axpy.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"
#include "dune/fufem/boundarypatch.hh"

//! Generic global assembler for operators on a gridview
template <class TestBasis, class AnsatzBasis>
class BoundaryOperatorAssembler
{
    private:
        typedef typename TestBasis::GridView GridView;
        //typedef BoundaryPatchType<GridView> BoundaryPatchType

    public:
        //! create assembler for grid
        BoundaryOperatorAssembler(const TestBasis& tBasis, const AnsatzBasis& aBasis, const BoundaryPatch<GridView>& boundaryPatch) :
            tBasis_(tBasis),
            aBasis_(aBasis),
            boundaryPatch_(boundaryPatch)
        {}


        template <class LocalAssemblerType>
        void addIndices(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices, const bool lumping=false) const
        {
            if (lumping)
                addIndicesStaticLumping<LocalAssemblerType,true>(localAssembler, indices);
            else
                addIndicesStaticLumping<LocalAssemblerType,false>(localAssembler, indices);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void addEntries(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            if (lumping)
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,true>(localAssembler, A);
            else
                addEntriesStaticLumping<LocalAssemblerType,GlobalMatrixType,false>(localAssembler, A);
        }


        template <class LocalAssemblerType, class GlobalMatrixType>
        void assemble(LocalAssemblerType& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            int rows = tBasis_.size();
            int cols = aBasis_.size();

            Dune::MatrixIndexSet indices(rows, cols);

            addIndices(localAssembler, indices, lumping); //indices will later contain all indices of constraint points on our boundary

            indices.exportIdx(A);
            A=0.0;

            addEntries(localAssembler, A, lumping);

            return;
        }


    private:

        template <class LocalAssemblerType, bool lumping>
        void addIndicesStaticLumping(LocalAssemblerType& localAssembler, Dune::MatrixIndexSet& indices) const
        {
            typedef typename BoundaryPatch<GridView>::iterator BoundaryIterator; //iterates over boundary elements
            typedef typename LocalAssemblerType::BoolMatrix BoolMatrix;
            typedef typename TestBasis::LinearCombination LinearCombination;

            BoundaryIterator it = boundaryPatch_.begin();
            BoundaryIterator end = boundaryPatch_.end();
            for (; it != end; ++it)
            {
                const auto inside = it->inside();

                // get shape functions
                const typename TestBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(inside);
                const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(inside);

                BoolMatrix localIndices(tFE.localBasis().size(), aFE.localBasis().size());
                localAssembler.indices(it, localIndices, tFE, aFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int rowIndex = tBasis_.index(inside, i);
                    const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                    bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                    for (size_t j=0; j<aFE.localBasis().size(); ++j)
                    {
                        if (localIndices[i][j])
                        {
                            if (lumping)
                            {
                                if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        indices.add(rowConstraints[rw].index, rowConstraints[rw].index);
                                }
                                else
                                    indices.add(rowIndex, rowIndex);
                            }
                            else
                            {
                                int colIndex = aBasis_.index(inside, j);
                                const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                if (rowIsConstrained and colIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                            indices.add(rowConstraints[rw].index, colConstraints[cw].index);
                                    }
                                }
                                else if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        indices.add(rowConstraints[rw].index, colIndex);
                                }
                                else if (colIsConstrained)
                                {
                                    for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                        indices.add(rowIndex, colConstraints[cw].index);
                                }
                                else
                                    indices.add(rowIndex, colIndex);
                            }
                        }
                    }
                }
            }
        }


        template <class LocalAssemblerType, class GlobalMatrixType, bool lumping>
        void addEntriesStaticLumping(LocalAssemblerType& localAssembler, GlobalMatrixType& A) const
        {
            typedef typename BoundaryPatch<GridView>::iterator BoundaryIterator;
            typedef typename LocalAssemblerType::LocalMatrix LocalMatrix;
            typedef typename TestBasis::LinearCombination LinearCombination;

            BoundaryIterator it = boundaryPatch_.begin();
            BoundaryIterator end = boundaryPatch_.end();
            for (; it != end; ++it)
            {
                const auto inside = it->inside();

                // get shape functions
                const typename TestBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(inside);
                const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(inside);

                LocalMatrix localA(tFE.localBasis().size(), aFE.localBasis().size());
                localAssembler.assemble(it, localA, tFE, aFE);

                for (size_t i=0; i<tFE.localBasis().size(); ++i)
                {
                    int rowIndex = tBasis_.index(inside, i);
                    const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                    bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                    for (size_t j=0; j<aFE.localBasis().size(); ++j)
                    {

                        if (localA[i][j].infinity_norm()!=0.0)
                        {
                            if (lumping)
                            {
                                if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        A[rowConstraints[rw].index][rowConstraints[rw].index].axpy(rowConstraints[rw].factor, localA[i][j]);
//                                        Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][rowConstraints[rw].index], rowConstraints[rw].factor, localA[i][j]);
                                }
                                else
                                    A[rowIndex][rowIndex].axpy(1.0, localA[i][j]);
//                                    Dune::MatrixVector::addProduct(A[rowIndex][rowIndex], 1.0, localA[i][j]);
                            }
                            else
                            {
                                int colIndex = aBasis_.index(inside, j);
                                const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                                bool colIsConstrained = aBasis_.isConstrained(colIndex);
                                if (rowIsConstrained and colIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    {
                                        for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                            A[rowConstraints[rw].index][colConstraints[cw].index].axpy(rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
//                                            Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                    }
                                }
                                else if (rowIsConstrained)
                                {
                                    for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                        A[rowConstraints[rw].index][colIndex].axpy(rowConstraints[rw].factor, localA[i][j]);
//                                        Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                                }
                                else if (colIsConstrained)
                                {
                                    for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                        A[rowIndex][colConstraints[cw].index].axpy(colConstraints[cw].factor, localA[i][j]);
//                                        Dune::MatrixVector::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                                }
                                else
                                    A[rowIndex][colIndex].axpy(1.0, localA[i][j]);
//                                    Dune::MatrixVector::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                            }
                        }
                    }
                }
            }
        }


        const TestBasis& tBasis_;
        const AnsatzBasis& aBasis_;
        const BoundaryPatch<GridView>& boundaryPatch_;
};

#endif

