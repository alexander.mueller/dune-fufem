#ifndef INTEGRAL_OPERATOR_ASSEMBLER_HH
#define INTEGRAL_OPERATOR_ASSEMBLER_HH

#include <dune/istl/matrix.hh>

#include <dune/matrix-vector/axpy.hh>

#include "dune/fufem/functionspacebases/functionspacebasis.hh"

//! Generic global assembler for operators on a gridview
template <class TestBasis, class AnsatzBasis>
class IntegralOperatorAssembler
{
    public:
        //! create assembler for grid
        IntegralOperatorAssembler(const TestBasis& tBasis, const AnsatzBasis& aBasis) :
            tBasis_(tBasis),
            aBasis_(aBasis)
        {}


        template <class LocalIntegralAssemblerType, class GlobalMatrixType>
        void addEntries(LocalIntegralAssemblerType& localAssembler, GlobalMatrixType& A, const bool isSymmetric=false) const
        {
            if (isSymmetric)
                addEntriesStaticSymmetryFlag<LocalIntegralAssemblerType,GlobalMatrixType,true>(localAssembler, A);
            else
                addEntriesStaticSymmetryFlag<LocalIntegralAssemblerType,GlobalMatrixType,false>(localAssembler, A);
        }


        template <class LocalIntegralAssemblerType, class GlobalMatrixType>
        void assemble(LocalIntegralAssemblerType& localAssembler, GlobalMatrixType& A, const bool isSymmetric=false) const
        {
            int rows = tBasis_.size();
            int cols = aBasis_.size();

            A.setSize(rows, cols);
            A=0.0;

            addEntries(localAssembler, A, isSymmetric);

            return;
        }


    protected:


        template <class LocalIntegralAssemblerType, class GlobalMatrixType, bool isSymmetric>
        void addEntriesStaticSymmetryFlag(LocalIntegralAssemblerType& localAssembler, GlobalMatrixType& A) const
        {
            typedef typename TestBasis::GridView::template Codim<0>::Iterator TestElementIterator;
            typedef typename AnsatzBasis::GridView::template Codim<0>::Iterator AnsatzElementIterator;
            typedef typename LocalIntegralAssemblerType::LocalMatrix LocalMatrix;
            typedef typename TestBasis::LinearCombination LinearCombination;

            // iterate over all elements for test fe space
            TestElementIterator tIt = tBasis_.getGridView().template begin<0>();
            TestElementIterator tEnd = tBasis_.getGridView().template end<0>();
            for (; tIt != tEnd; ++tIt)
            {
                // get test shape functions
                const typename TestBasis::LocalFiniteElement& tFE = tBasis_.getLocalFiniteElement(*tIt);

                // iterate over all elements for ansatz fe space
                AnsatzElementIterator aIt = aBasis_.getGridView().template begin<0>();
                AnsatzElementIterator aEnd = aBasis_.getGridView().template end<0>();
                for (; aIt != aEnd; ++aIt)
                {
                    // get ansatz shape functions
                    const typename AnsatzBasis::LocalFiniteElement& aFE = aBasis_.getLocalFiniteElement(*aIt);

                    LocalMatrix localA(tFE.localBasis().size(), aFE.localBasis().size());
                    localAssembler.assemble(*tIt, tFE, *aIt, aFE, localA);

                    for (size_t i=0; i<tFE.localBasis().size(); ++i)
                    {
                        int rowIndex = tBasis_.index(*tIt, i);
                        const LinearCombination& rowConstraints = tBasis_.constraints(rowIndex);
                        bool rowIsConstrained = tBasis_.isConstrained(rowIndex);
                        for (size_t j=0; j<aFE.localBasis().size(); ++j)
                        {
                            int colIndex = aBasis_.index(*aIt, j);
                            const LinearCombination& colConstraints = aBasis_.constraints(colIndex);
                            bool colIsConstrained = aBasis_.isConstrained(colIndex);
                            if (rowIsConstrained and colIsConstrained)
                            {
                                for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                {
                                    for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                        Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colConstraints[cw].index], rowConstraints[rw].factor * colConstraints[cw].factor, localA[i][j]);
                                }
                            }
                            else if (rowIsConstrained)
                            {
                                for (size_t rw=0; rw<rowConstraints.size(); ++rw)
                                    Dune::MatrixVector::addProduct(A[rowConstraints[rw].index][colIndex], rowConstraints[rw].factor, localA[i][j]);
                            }
                            else if (colIsConstrained)
                            {
                                for (size_t cw=0; cw<colConstraints.size(); ++cw)
                                    Dune::MatrixVector::addProduct(A[rowIndex][colConstraints[cw].index], colConstraints[cw].factor, localA[i][j]);
                            }
                            else
                                Dune::MatrixVector::addProduct(A[rowIndex][colIndex], 1.0, localA[i][j]);
                        }
                    }
                }
            }
        }

        const TestBasis& tBasis_;
        const AnsatzBasis& aBasis_;
};

#endif

