#ifndef ASSEMBLER_HH
#define ASSEMBLER_HH

#include "dune/fufem/assemblers/operatorassembler.hh"
#include "dune/fufem/assemblers/functionalassembler.hh"
#include "dune/fufem/assemblers/boundaryfunctionalassembler.hh"

#include "dune/fufem/boundarypatch.hh"

//! Generic global assembler for operators and functionals
template <class TestBasis, class AnsatzBasis>
class Assembler
{
    public:
        //! create assembler
        Assembler(const TestBasis& tBasis, const AnsatzBasis& aBasis) :
            tBasis_(tBasis),
            aBasis_(aBasis)
        {}

        template <class LocalAssemblerType, class GlobalMatrixType>
        void assembleOperator(LocalAssemblerType&& localAssembler, GlobalMatrixType& A, const bool lumping=false) const
        {
            const OperatorAssembler<TestBasis, AnsatzBasis> operatorAssembler(tBasis_, aBasis_);
            operatorAssembler.assemble(localAssembler, A, lumping);
        }

        template <class LocalFunctionalAssemblerType, class GlobalVectorType>
        void assembleFunctional(LocalFunctionalAssemblerType& localAssembler, GlobalVectorType& b, bool initializeVector=true) const
        {
            const FunctionalAssembler<TestBasis> functionalAssembler(tBasis_);
            functionalAssembler.assemble(localAssembler, b, initializeVector);
        }

        template <class LocalBoundaryFunctionalAssemblerType, class GlobalVectorType>
        void assembleBoundaryFunctional(LocalBoundaryFunctionalAssemblerType& localAssembler,
                                        GlobalVectorType& b,
                                        const BoundaryPatch<typename TestBasis::GridView>& boundaryPatch,
                                        bool initializeVector=true) const
        {
            const BoundaryFunctionalAssembler<TestBasis> boundaryFunctionalAssembler(tBasis_, boundaryPatch);
            boundaryFunctionalAssembler.assemble(localAssembler, b, initializeVector);
        }

    private:
        const TestBasis& tBasis_;
        const AnsatzBasis& aBasis_;
};

template <class TestBasis, class AnsatzBasis>
auto assembler(const TestBasis& tBasis, const AnsatzBasis& aBasis) {
  return Assembler<TestBasis, AnsatzBasis>(tBasis, aBasis);
}

#endif

