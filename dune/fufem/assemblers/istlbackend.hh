// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_ASSEMBLERS_ISTLBACKEND_HH
#define DUNE_FUFEM_ASSEMBLERS_ISTLBACKEND_HH


#include <dune/fufem/backends/istlmatrixbackend.hh>

#warning This file is deprecated. Please include dune/fufem/backends/istlmatrixbackend.hh instead.



#endif // DUNE_FUFEM_ASSEMBLERS_ISTLBACKEND_HH
