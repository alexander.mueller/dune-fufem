#ifndef FUNCTIONINTEGRATOR_HH
#define FUNCTIONINTEGRATOR_HH

#include <dune/common/fvector.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/functions/gridfunctions/gridviewfunction.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/fufem/quadraturerules/quadraturerulecache.hh>

/**  \brief provides static methods for numerical integration
  *
  */
class FunctionIntegrator
{
    public:

        /**  \brief numerically integrates a given function
          *
          *  Numerically integrates the given \a integrand over the domain covered by the
          *  given GridView using a Dune::QuadratureRule of order \a quad_order on the grid specified by the GridView.
          *
          *  \tparam GridView the type of the GridView provided
          *  \tparam FunctionType the type of the function provided, needs to provide method <tt>void evalall(...)</tt> and the FunctionType's rangetype has to be RT
          *  \param gv the GridView describing the domain of integration and the Grid for quadrature
          *  \param integrand the function to be integrated
          *  \param quad_order the order of the quadrature rule employed
          */
        template <class GridView, class FunctionType>
        static auto integrate(const GridView& gv, const FunctionType& integrand, const int quad_order)
        {
            using GlobalDomain = typename GridView::template Codim<0>::Geometry::GlobalCoordinate;
            using LocalDomain = typename GridView::template Codim<0>::Geometry::LocalCoordinate;
            using RangeType = std::decay_t<std::invoke_result_t<FunctionType, GlobalDomain>>;
            using FieldType = typename Dune::template FieldTraits<RangeType>::field_type;

            RangeType ret(0.0);
            RangeType f_pos(0.0);

            auto gridFunctionIntegrand = Dune::Functions::makeGridViewFunction(integrand, gv);
            auto localIntegrand = localFunction(gridFunctionIntegrand);

            for (const auto& element : elements(gv))
            {
                auto&& geometry = element.geometry();

                localIntegrand.bind(element);

                QuadratureRuleKey quadKey(element.type(), quad_order);

                const auto& quad = QuadratureRuleCache<FieldType, LocalDomain::dimension>::rule(quadKey);

                // loop over quadrature points
                for (size_t pt=0; pt < quad.size(); ++pt)
                {
                    // get quadrature point
                    const auto& quadPos = quad[pt].position();

                    // get integration factor
                    const double integrationElement = geometry.integrationElement(quadPos);

                    // compute values of function
                    f_pos = localIntegrand(quadPos);

                    // add to integral
                    ret.axpy(quad[pt].weight()*integrationElement, f_pos);
                }
            }

            return ret;
        }

        /**  \brief numerically integrates a gridfunction given by a coefficient vector
          *
          *  Numerically integrates the gridfunction represented by a coefficient vector in a given basis
          *  using a Dune::QuadratureRule of order quad_order.
          *
          *  \tparam RangeType Range type for the grid function
          *  \tparam BasisType the functionspace basis for the coefficient representation
          *  \tparam VectorType the type of the coefficient vector
          *  \param basis the functionspace basis of the gridfunction
          *  \param integrandCoeff the coefficient vector
          *  \param quad_order the order of the quadrature rule employed
          */
        template <class RangeType, class BasisType, class VectorType>
        static auto integrateDiscreteFunction(const BasisType& basis, const VectorType& integrandCoeff, const int quad_order)
        {
            auto integrand = Dune::Functions::makeDiscreteGlobalBasisFunction<RangeType>(basis, integrandCoeff);
            return integrate(basis.getGridView(), integrand, quad_order);
        }
};


#endif

