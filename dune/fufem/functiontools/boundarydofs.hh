// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set ts=8 sw=4 et sts=4:
#ifndef DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH
#define DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH

#include <type_traits>

#include <dune/common/bitsetvector.hh>
#include <dune/common/version.hh>

#include <dune/fufem/boundarypatch.hh>

#include <dune/functions/backends/concepts.hh>
#include <dune/functions/backends/istlvectorbackend.hh>
#include <dune/functions/functionspacebases/subentitydofs.hh>


/** \brief For a given basis and boundary patch, determine all degrees
    of freedom on the patch.

    A bit of boundaryDofs is set if corresponding dof belongs to the boundary patch.

    \param boundaryDofs BitSetVector corresponding to the basis DOFs
    \param sfinae Dummy parameter to instantiate this method only for dune-functions bases
*/
template <class GridView, class Basis, class BitSetVector>
void constructBoundaryDofs(const BoundaryPatch<GridView>& boundaryPatch,
                           const Basis& basis,
                           BitSetVector& boundaryDofs,
                           typename Basis::LocalView* sfinae = nullptr)
{
    // Check consistency of the input
    static_assert((std::is_same<GridView, typename Basis::GridView>::value),
                       "BoundaryPatch and global basis must be for the same grid view!");

    // Small helper function to wrap vectors using istlVectorBackend
    // if they do not already satisfy the VectorBackend interface.
    auto toVectorBackend = [&](auto& v) -> decltype(auto) {
        if constexpr (Dune::models<Dune::Functions::Concept::VectorBackend<Basis>, decltype(v)>()) {
            return v;
        } else {
            return Dune::Functions::istlVectorBackend(v);
        }
    };

    //////////////////////////////////////////////////////////
    //   Init output bitfield
    //////////////////////////////////////////////////////////

    // ensure IstlVectorBackend interface
    auto boundaryDofsBackend = toVectorBackend(boundaryDofs);

    boundaryDofsBackend.resize(basis);
    boundaryDofsBackend = false;

    auto localView = basis.localView();
    auto seDOFs = Dune::Functions::subEntityDOFs(basis);
    for(const auto& intersection : boundaryPatch)
    {
        localView.bind(intersection.inside());
        for(auto localIndex: seDOFs.bind(localView, intersection))
            boundaryDofsBackend[localView.index(localIndex)] = true;
    }
}


/** \brief For a given basis and boundary patch, determine all degrees
    of freedom on the patch.

    Same method as above, but this time for dune-fufem bases.
*/
template <class GridView, class Basis, int blocksize>
void constructBoundaryDofs(const BoundaryPatch<GridView>& boundaryPatch,
                           const Basis& basis,
                           Dune::BitSetVector<blocksize>& boundaryDofs,
                           typename Basis::LocalFiniteElement* sfinae=nullptr)
{
    // Check consistency of the input
    static_assert((std::is_same<GridView, typename Basis::GridView>::value),
                       "BoundaryPatch and global basis must be for the same grid view!");

    // ////////////////////////////////////////////////////////
    //   Init output bitfield
    // ////////////////////////////////////////////////////////

    boundaryDofs.resize(basis.size());
    boundaryDofs.unsetAll();


    for (auto it = boundaryPatch.begin(); it != boundaryPatch.end(); ++it) {

        const auto& inside = it->inside();
        const auto& localCoefficients = basis.getLocalFiniteElement(inside).localCoefficients();

        for (size_t i=0; i<localCoefficients.size(); i++) {

            // //////////////////////////////////////////////////
            //   Test whether dof is on the boundary face
            // //////////////////////////////////////////////////

            unsigned int entity = localCoefficients.localKey(i).subEntity();
            unsigned int codim  = localCoefficients.localKey(i).codim();

            if (it.containsInsideSubentity(entity, codim))
                boundaryDofs[ basis.index(inside, i) ] = true;
        }
    }
}

#endif   // DUNE_FUFEM_FUNCTIONTOOLS_BOUNDARYDOFS_HH
