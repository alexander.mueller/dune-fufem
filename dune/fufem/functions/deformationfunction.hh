#ifndef DEFORMATION_FUNCTION_HH
#define DEFORMATION_FUNCTION_HH

#include <memory>

#include <dune/common/fvector.hh>

#include <dune/grid/geometrygrid/coordfunction.hh>

#include <dune/geometry/referenceelements.hh>

#include <dune/localfunctions/lagrange/pqkfactory.hh>


/** \brief Function that computes the deformed position from a given host gridview. This can be used inside of GeometryGrid
 *
 * This function only works correctly if the evaluate function is only called for elements of the given GridView.
 *
 *  \tparam GridView - The grid view of the undeformed grid.
 *  \tparam CoefficientVectorType - The type the vertex coefficients are stored in.
 *
*/
template <class GridView, class CoefficientVectorType>
class DeformationFunction  :
 public Dune::DiscreteCoordFunction<typename GridView::ctype, GridView::dimension,
                                        DeformationFunction<GridView,CoefficientVectorType> >
{
    enum {dim = GridView::dimension};

    typedef typename GridView::ctype ctype;
    typedef DeformationFunction<GridView,CoefficientVectorType> This;
    typedef Dune::DiscreteCoordFunction<ctype, dim, This > Base;
public:
    typedef typename Base::RangeVector RangeVector;

    DeformationFunction(const GridView& gridView)
    {
        gridView_ = std::unique_ptr<GridView>(new GridView(gridView));
        deformation_.resize(gridView_->size(dim));
        deformation_ = 0;
    }


    DeformationFunction(const GridView& gridView,
                        const CoefficientVectorType& deformation)
        : deformation_(deformation)
    {
        gridView_ = std::unique_ptr<GridView>(new GridView(gridView));

        if(gridView_->size(dim) != deformation_.size())
            DUNE_THROW(Dune::Exception,"The deformation coefficient vector doesn't match the gridview!");
    }

    virtual ~DeformationFunction() {}

    virtual void setGridView(const GridView& gridView) {
        gridView_ = std::unique_ptr<GridView>(new GridView(gridView));
    }

    //! Change the deformation vector
    void setDeformation(const CoefficientVectorType& deformation)
    {
        if (deformation_.size() != deformation.size())
            std::cout<<"Warning chanign deformation size "<<deformation_.size()<<" to "<<deformation.size()<<std::endl;
        deformation_ = deformation;
    }

    //! Evaluate function at a host vertex (corner param is redundant but prescribed by the interface...)
    virtual void evaluate ( const typename GridView::template Codim<dim>::Entity& hostVertex, unsigned int corner,
                    Dune::FieldVector<ctype,dim> &y ) const
    {
        const typename GridView::IndexSet& indexSet = gridView_->indexSet();
        int idx = indexSet.index(hostVertex);

        y  = hostVertex.geometry().corner(0) + deformation_[idx];
    }

    //! Evaluate function at a corner of a host entity
    virtual void evaluate (const typename GridView::template Codim<0>::Entity& hostEntity, unsigned int corner,
                    typename Base::RangeVector& y ) const
    {
        const typename GridView::IndexSet& indexSet = gridView_->indexSet();
        int idx = indexSet.subIndex(hostEntity, corner,dim);
        y  = hostEntity.geometry().corner(corner) + deformation_[idx];
    }

    //! Adapt to changes in the host grid
    void adapt() {
        if ((int) deformation_.size() != gridView_->size(dim)) {
            std::cout<<"Warning removing all data from DeformationFunction! \n";
            deformation_.resize(gridView_->size(dim));
            deformation_ = 0;
        }
    }

    const CoefficientVectorType& getDeformation() {return deformation_;}
protected:
    //! The gridview of the undeformed grid
    std::unique_ptr<GridView> gridView_;

    //! The coefficient vector of the displacements of the vertices of the gridview
    CoefficientVectorType deformation_;
};


/** \brief Function that computes the deformed position from a given host gridview. This can be used inside of GeometryGrid
 *
 *  \tparam GridView - The grid view of the undeformed grid.
 *  \tparam CoefficientVectorType - The type the vertex coefficients are stored in.
 *
*/
template <class GridView, class CoefficientVectorType>
class DeformationHierarchyFunction  :
 public DeformationFunction<GridView,CoefficientVectorType>
{
    enum {dim = GridView::dimension};

    typedef typename GridView::ctype ctype;
    typedef typename CoefficientVectorType::field_type field_type;
    typedef DeformationFunction<GridView,CoefficientVectorType> Base;
public:

    DeformationHierarchyFunction(const GridView& gridView) :
        Base(gridView)
    {
        setupIndexMapping();
    }


    DeformationHierarchyFunction(const GridView& gridView,
                        const CoefficientVectorType& deformation) :
        Base(gridView, deformation)
    {
        setupIndexMapping();
    }

    virtual void setGridView(const GridView& gridView) {
        Base::setGridView(gridView);
        setupIndexMapping();
    }
    // TODO Implement evaluate method for corners...

    //! Evaluate function at a corner of a host entity
    void evaluate (const typename GridView::template Codim<0>::Entity& hostEntity, unsigned int corner,
                    typename Base::RangeVector& y ) const
    {
        if (hostEntity.isLeaf()) {
            Base::evaluate(hostEntity,corner,y);
            return;
        }
        const auto& indexSet = this->gridView_->grid().levelIndexSet(hostEntity.level());

        int idx = indexSet.subIndex(hostEntity, corner,dim);
        y  = hostEntity.geometry().corner(corner) + this->deformation_[fineIndex_[hostEntity.level()][idx]];
    }

private:
    //! Index mapping from non-leaf elements to the corresponding leaf index
    std::vector<std::vector<int> > fineIndex_;

    //! Setup the mapping that maps level index to the corresponding index in the leaf grid
    void setupIndexMapping() {

        int maxLevel = this->gridView_->grid().maxLevel();

        // all elements are leaf
        if (maxLevel==0)
            return;

        fineIndex_.resize(maxLevel+1);

        const typename GridView::IndexSet& leafIndex = this->gridView_->indexSet();

        // A factory for the P1 shape functions
        typedef typename Dune::PQkLocalFiniteElementCache<ctype, field_type, dim, 1> P1FECache;
        typedef typename P1FECache::FiniteElementType FEType;
        P1FECache p1FECache;

        const typename GridView::Grid& grid = this->gridView_->grid();
        typedef typename GridView::Grid::LevelGridView::template Codim<0>::Iterator LevelElementIterator;
        typedef typename LevelElementIterator::Entity EntityType;
        typedef typename EntityType::HierarchicIterator HierarchicIterator;

        for (int i=maxLevel; i>=0; i--) {
            fineIndex_[i].resize(grid.size(i,dim),-1);

            const auto& levelIndex = grid.levelIndexSet(i);
            int fineLevel = (i==maxLevel) ? i : (i+1);
            const auto& fineIndex = grid.levelIndexSet(fineLevel);

            const auto& lv = grid.levelGridView(i);
            for (const auto& e: elements(lv)) {

                // all vertices of leaf elements are leaf
                if (e.isLeaf()) {

                    auto ref
                        = Dune::ReferenceElements<ctype, dim>::general(e.type());
                    for (int j=0; j<ref.size(dim); j++)
                        fineIndex_[i][levelIndex.subIndex(e,j,dim)] = leafIndex.subIndex(e,j,dim);
                    continue;
                }

                // Get local finite element
                const FEType& coarseBaseSet = p1FECache.get(e.type());

                const size_t numCoarseBaseFct = coarseBaseSet.localBasis().size();

                // preallocate vector for function evaluations
                std::vector<Dune::FieldVector<field_type,1> > values(numCoarseBaseFct);

                HierarchicIterator fIt    = e.hbegin(i+1);
                HierarchicIterator fEndIt = e.hend(i+1);

                for (; fIt != fEndIt; ++fIt) {

                    if (fIt->level()==i)
                        continue;

                    auto fineRefElement
                        = Dune::ReferenceElements<ctype, dim>::general(fIt->type());

                    const auto& fGeometryInFather = fIt->geometryInFather();

                    // Get local finite element
                    const FEType& fineBaseSet = p1FECache.get(fIt->type());

                    const size_t numFineBaseFct = fineBaseSet.localBasis().size();

                    for (size_t j=0; j<numFineBaseFct; j++)
                    {
                        //compute position of child element node in father element
                        const Dune::LocalKey& jLocalKey = fineBaseSet.localCoefficients().localKey(j);

                        int globalLeaf = fineIndex_[fineLevel][fineIndex.subIndex(*fIt, jLocalKey.subEntity(), dim)];

                        Dune::FieldVector<ctype, dim> fineBasePosition = fineRefElement.position(jLocalKey.subEntity(), dim);
                        Dune::FieldVector<ctype, dim> local = fGeometryInFather.global(fineBasePosition);

                        // Evaluate coarse grid base functions
                        coarseBaseSet.localBasis().evaluateFunction(local, values);

                        for (size_t k=0; k<numCoarseBaseFct; k++)
                        {
                            if (values[k] > 0.9999)
                            {
                                const Dune::LocalKey& coarseKey = coarseBaseSet.localCoefficients().localKey(k);
                                int globalCoarse = levelIndex.subIndex(e, coarseKey.subEntity(), dim);
                                fineIndex_[i][globalCoarse] = globalLeaf;
                            }
                        }
                    }

                }
            }
        }
    }
};




#endif
