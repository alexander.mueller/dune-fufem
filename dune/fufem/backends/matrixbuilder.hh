// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_FUFEM_BACKENDS_MATRIXBUILDER_HH
#define DUNE_FUFEM_BACKENDS_MATRIXBUILDER_HH

#include <array>
#include <vector>

#include <dune/common/indices.hh>
#include <dune/common/hybridutilities.hh>

#include <dune/istl/matrix.hh>
#include <dune/istl/bcrsmatrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/multitypeblockvector.hh>
#include <dune/istl/multitypeblockmatrix.hh>

namespace Dune::Fufem {



/**
 * \brief Helper class for building matrix pattern
 *
 * This needs to be spezialized for the specific
 * matrix types.
 */
template<class Matrix>
class MatrixBuilder;



/**
 * \brief Helper class for building matrix pattern
 *
 * This spezialization for BCRSMatrix essentially
 * forwards to Dune::MatrixIndexSet.
 */
template<class T, class A>
class MatrixBuilder<Dune::BCRSMatrix<T, A>>
{
public:

  using Matrix = Dune::BCRSMatrix<T, A>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    indices_.resize(rowSizeInfo.size(), colSizeInfo.size());
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_.add(rowIndex[0], colIndex[0]);
  }

  void setupMatrix()
  {
    indices_.exportIdx(matrix_);
  }

private:
  Dune::MatrixIndexSet indices_;
  Matrix& matrix_;
};



/**
 * \brief Helper class for building matrix pattern
 *
 *  Version for MultiTypeBlockMatrix with BCRSMatrix-like blocks.
 *  It uses a 2D std::array of MatrixIndexSets
 */
template<class Row0, class... Rows>
class MatrixBuilder<MultiTypeBlockMatrix<Row0,Rows...>>
{
public:

  using Matrix = MultiTypeBlockMatrix<Row0,Rows...>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    for(size_t i=0; i<rowBlocks_; i++)
      for(size_t j=0; j<colBlocks_; j++)
        indices_[i][j].resize(rowSizeInfo.size({i}), colSizeInfo.size({j}));
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_[rowIndex[0]][colIndex[0]].add(rowIndex[1], colIndex[1]);
  }

  void setupMatrix()
  {
    Hybrid::forEach(Hybrid::integralRange(Dune::index_constant<rowBlocks_>()), [&](auto&& i) {
      Hybrid::forEach(Hybrid::integralRange(Dune::index_constant<colBlocks_>()), [&](auto&& j) {
        indices_[i][j].exportIdx(matrix_[i][j]);
      });
    });

  }

private:
  //! number of multi type rows
  static constexpr size_t rowBlocks_ = Matrix::N();
  //! number of multi type cols (we assume the matrix is well-formed)
  static constexpr size_t colBlocks_ = Matrix::M();
  //! 2D array of IndexSets
  std::array<std::array<Dune::MatrixIndexSet,colBlocks_>,rowBlocks_> indices_;
  Matrix& matrix_;
};



template<class T, class A>
class MatrixBuilder<Dune::Matrix<Dune::BCRSMatrix<T, A>>>
{
public:

  using Matrix = Dune::Matrix<Dune::BCRSMatrix<T, A>>;

  MatrixBuilder(Matrix& matrix) :
    matrix_(matrix)
  {}

  template<class RowSizeInfo, class ColSizeInfo>
  void resize(const RowSizeInfo& rowSizeInfo, const ColSizeInfo& colSizeInfo)
  {
    auto rowBlocks = rowSizeInfo.size();
    auto colBlocks = colSizeInfo.size();
    indices_.resize(rowBlocks);
    for(size_t i=0; i<rowBlocks; i++)
    {
      indices_[i].resize(colBlocks);
      for(size_t j=0; j<colBlocks; j++)
        indices_[i][j].resize(rowSizeInfo.size({i}), colSizeInfo.size({j}));
    }
  }

  template<class RowIndex, class ColIndex>
  void insertEntry(const RowIndex& rowIndex, const ColIndex& colIndex)
  {
    indices_[rowIndex[0]][colIndex[0]].add(rowIndex[1], colIndex[1]);
  }

  void setupMatrix()
  {
    auto rowBlocks = indices_.size();
    auto colBlocks = indices_[0].size();
    matrix_.setSize(rowBlocks, colBlocks);
    for(size_t i=0; i<rowBlocks; i++)
      for(size_t j=0; j<colBlocks; j++)
        indices_[i][j].exportIdx(matrix_[i][j]);
  }

private:
  std::vector<std::vector<Dune::MatrixIndexSet>> indices_;
  Matrix& matrix_;
};



} // namespace Dune::Fufem



#endif // DUNE_FUFEM_BACKENDS_MATRIXBUILDER_HH
