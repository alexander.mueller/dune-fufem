#ifndef REFINED_QUADRATURE_RULE_HH
#define REFINED_QUADRATURE_RULE_HH

/**
   @file
   @brief

   @author
 */

#include <dune/common/fmatrix.hh>
#include <dune/fufem/quadraturerules/compositequadraturerule.hh>


template <class ct, int dim>
class RefinedSimplexQuadratureRule:
    public CompositeQuadratureRule<ct,dim>
{
    protected:
        typedef typename Dune::QuadratureRule<ct,dim> Quad;
        typedef typename Dune::QuadraturePoint<ct,dim> QuadPoint;
        typedef typename Dune::FieldVector<ct,dim> Point;
    public:
        RefinedSimplexQuadratureRule(const Quad& quad)
        : CompositeQuadratureRule<ct,dim>(quad,1)
        {}
};

/** \brief Specialization for the 3d case
 * 
 * The refinement used for quadrature rules must match the refinement for
 * shape functions.  This is not a problem for 1d and 2d, where the refinement
 * is canonical.  In 3d, however, there is a choice, and currently I don't know
 * whether the refinement of VirtualRefinement matches the one of the shape
 * functions.  Therefore I keep this specialization until I have the time to
 * check.
 */
template <class ct>
class RefinedSimplexQuadratureRule<ct,3> :
    public Dune::QuadratureRule<ct,3>
{
    protected:
        typedef typename Dune::QuadratureRule<ct,3> Quad;
        typedef typename Dune::QuadraturePoint<ct,3> QuadPoint;
        typedef typename Dune::FieldVector<ct,3> Point;

    public:

        RefinedSimplexQuadratureRule(const Quad& quad)
        {
            Point p,pp;
            for(size_t i=0; i<quad.size(); ++i)
            {
                double w = quad[i].weight() / 8.0;

                // die vier abgeschnittenen Ecken
                p = quad[i].position();
                p *= 0.5;
                this->push_back(QuadPoint(p, w));

                p = quad[i].position();
                p *= 0.5;
                p[0] += 0.5;
                this->push_back(QuadPoint(p, w));

                p = quad[i].position();
                p *= 0.5;
                p[1] += 0.5;
                this->push_back(QuadPoint(p, w));

                p = quad[i].position();
                p *= 0.5;
                p[2] += 0.5;
                this->push_back(QuadPoint(p, w));

                // die aus dem übrigen Oktaeder entstehenden 4 Tetraeder
                Dune::FieldMatrix<double,3,3> A(0.0);
                A[0][0] = -0.5;
                A[0][1] = -0.5;
                A[1][0] =  0.5;
                A[2][1] =  0.5;
                A[2][2] =  0.5;
                pp = quad[i].position();
                A.mv(pp,p);
                p[0] += 0.5;
                this->push_back(QuadPoint(p, w));

                A = 0.0;
                A[0][0] = -0.5;
                A[1][1] = -0.5;
                A[1][2] = -0.5;
                A[2][2] =  0.5;
//                pp = quad[i].position();
                A.mv(pp,p);
                p[0] += 0.5;
                p[1] += 0.5;
                this->push_back(QuadPoint(p, w));

                A = 0.0;
                A[0][1] =  0.5;
                A[1][0] = -0.5;
                A[1][1] = -0.5;
                A[2][0] =  0.5;
                A[2][1] =  0.5;
                A[2][2] =  0.5;
//                pp = quad[i].position();
                A.mv(pp,p);
                p[1] += 0.5;
                this->push_back(QuadPoint(p, w));

                A = 0.0;
                A[0][1] =  0.5;
                A[0][2] =  0.5;
                A[1][1] = -0.5;
                A[2][0] =  0.5;
                A[2][1] =  0.5;
//                pp = quad[i].position();
                A.mv(pp,p);
                p[1] += 0.5;
                this->push_back(QuadPoint(p, w));
            }
        }

};

#endif
