#ifndef BOUNDARY_ITERATOR_HH
#define BOUNDARY_ITERATOR_HH

#include "globalintersectioniterator.hh"



/** \brief Iterator of boundary intersections of grid view
 *
 * \tparam GridView The grid view on which this boundary patch lives
*/
template <class GridView>
class BoundaryIterator
    : public GlobalIntersectionIterator<GridView, BoundaryIterator<GridView> >
{
    typedef GlobalIntersectionIterator<GridView, BoundaryIterator<GridView> > Base;
    enum {dim=GridView::dimension};

    typedef typename GridView::IntersectionIterator IntersectionIterator;
    typedef typename GridView::template Codim<0>::Iterator ElementIterator;
    typedef typename GridView::template Codim<0>::Entity Element;

public:
    typedef typename Base::PositionFlag PositionFlag;

    /** \brief The type of objects we are iterating over */
    typedef typename Base::Intersection Intersection;

    /** \brief Create begin or end iterator for given grid view
     *
     * \param gridView Iterate over the intersections of this grid view
     * \param flag Create begin or end iterator for PositionFlag = begin or end, respectively.
     */
    BoundaryIterator(const GridView& gridView, PositionFlag flag) :
        Base(gridView, flag)
    {
        Base::initialIncrement();
    }

    /** \brief Create iterator for given grid view
     *
     * \param gridView Iterate over the intersections of this grid view
     * \param eIt Element iterator to the first element to consider
     * \param endEIt Element iterator after the last element to consider
     */
    BoundaryIterator(const GridView& gridView, const ElementIterator& eIt, const ElementIterator& endEIt) :
        Base(gridView, eIt, endEIt)
    {
        Base::initialIncrement();
    }

    bool skipElement(const Element& e)
    {
        return false;
    }

    bool skipIntersection(const Intersection& i)
    {
        return not(i.boundary());
    }
};



#endif

