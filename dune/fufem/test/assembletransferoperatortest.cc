#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/istl/bcrsmatrix.hh>

#include <dune/functions/functionspacebases/defaultglobalbasis.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/raviartthomasbasis.hh>
#include <dune/functions/functionspacebases/powerbasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#include <dune/fufem/assemblers/basisinterpolationmatrixassembler.hh>
#include "dune/fufem/test/common.hh"

using namespace Dune;

template<class Matrix, class Function, class FineBasis, class FineCoeff, class CoarseBasis, class CoarseCoeff>
double checkInterpolation(Matrix& matrix, const Function& f,
                        const FineBasis& fineBasis, FineCoeff& fineCoeffs,
                        const CoarseBasis& coarseBasis, CoarseCoeff& coarseCoeffs)
{
  // assemble the matrix
  assembleGlobalBasisTransferMatrix(matrix,coarseBasis,fineBasis);

  // interpolate f on the coarse grid
  Functions::interpolate(coarseBasis,coarseCoeffs,f);

  // interpolate f on the fine grid
  Functions::interpolate(fineBasis,fineCoeffs,f);

  // now we should have
  // fineCoeffs - matrix * coarseCoeffs == 0
  matrix.mmv(coarseCoeffs,fineCoeffs);

  return fineCoeffs.two_norm2();
}

template<int order, class GW0, class GW1>
bool testLagrange(const GW0& gw0, const GW1& gw1)
{
  // our test function for interpolation
  auto f = [](const auto& x)
  {
    // const function for order 0
    if ( order == 0 )
      return 1.;

    // for the other cases affine function
    auto y = 1.;
    for( auto xs : x )
      y += xs;
    return y;
  };

  auto coarseLagrange = Functions::BasisFactory::makeBasis(gw0, Functions::BasisFactory::lagrange<order>());
  auto fineLagrange = Functions::BasisFactory::makeBasis(gw1, Functions::BasisFactory::lagrange<order>());

  BCRSMatrix<FieldMatrix<double,1,1>> matrix;
  BlockVector<FieldVector<double,1>> coarseCoeffs, fineCoeffs;

  const auto diff = checkInterpolation(matrix, f, fineLagrange, fineCoeffs, coarseLagrange, coarseCoeffs);

  std::cout << " test Lagrange " << GW0::dimension << "D with order " << order  << " : error = " << diff << std::endl;

  return diff < 1e-12;
}


template<int order, class GW0, class GW1>
bool testPowerBasis(const GW0& gw0, const GW1& gw1)
{
  // our test function for interpolation
  auto f = [](const auto& x)
  {
    // const function for order 0
    if ( order == 0 )
      return FieldVector<double,3>{1.,1.,1.};

    // for the other cases affine function
    FieldVector<double,3> y;
    y = 1.;
    for( auto xs : x )
    {
      y[0] += xs;
      y[1] += 2*xs;
      y[2] += 3*xs;
    }
    return y;
  };

  using namespace Functions::BasisFactory;
  bool passed = true;
  {
    auto coarseLagrange = makeBasis(gw0, power<3>(lagrange<order>(),blockedInterleaved()));
    auto fineLagrange   = makeBasis(gw1, power<3>(lagrange<order>(),blockedInterleaved()));

    BCRSMatrix<FieldMatrix<double,3,3>> matrix;
    BlockVector<FieldVector<double,3>> coarseCoeffs, fineCoeffs;

    const auto diff = checkInterpolation(matrix, f, fineLagrange, fineCoeffs, coarseLagrange, coarseCoeffs);

    std::cout << " test PowerBasis blockedInterleaved " << GW0::dimension << "D with order " << order  << " : error = " << diff << std::endl;

    passed = passed and diff < 1e-12;
  }
  {
    auto coarseLagrange = makeBasis(gw0, power<3>(lagrange<order>(),flatInterleaved()));
    auto fineLagrange   = makeBasis(gw1, power<3>(lagrange<order>(),flatInterleaved()));

    BCRSMatrix<FieldMatrix<double,1,1>> matrix;
    BlockVector<FieldVector<double,1>> coarseCoeffs, fineCoeffs;

    const auto diff = checkInterpolation(matrix, f, fineLagrange, fineCoeffs, coarseLagrange, coarseCoeffs);

    std::cout << " test PowerBasis flatInterleaved " << GW0::dimension << "D with order " << order  << " : error = " << diff << std::endl;

    passed = passed and diff < 1e-12;
  }
  {
    auto coarseLagrange = makeBasis(gw0, power<3>(lagrange<order>(),flatLexicographic()));
    auto fineLagrange   = makeBasis(gw1, power<3>(lagrange<order>(),flatLexicographic()));

    BCRSMatrix<FieldMatrix<double,1,1>> matrix;
    BlockVector<FieldVector<double,1>> coarseCoeffs, fineCoeffs;

    const auto diff = checkInterpolation(matrix, f, fineLagrange, fineCoeffs, coarseLagrange, coarseCoeffs);

    std::cout << " test PowerBasis flatLexicographic " << GW0::dimension << "D with order " << order  << " : error = " << diff << std::endl;

    passed = passed and diff < 1e-12;
  }

  return passed;
}




template<int order0, int order1, class GW0, class GW1>
bool testOrder(const GW0& gw0, const GW1& gw1)
{
  // our test function for interpolation
  auto f = [](const auto& x)
  {
    FieldVector<double,3> y;
    y = 1.;
    for( auto xs : x )
    {
      y[0] += std::sin(xs);
      y[1] += 2*xs +5.;
      y[2] += 3*xs*xs;
    }
    return y;
  };

  using namespace Functions::BasisFactory;

  auto lagrange0 = makeBasis(gw0, power<3>(lagrange<order0>(),blockedInterleaved()));
  auto lagrange1 = makeBasis(gw1, power<3>(lagrange<order1>(),blockedInterleaved()));

  BCRSMatrix<FieldMatrix<double,3,3>> matrix;
  BlockVector<FieldVector<double,3>> coeffs0, coeffs1;

  const auto diff = checkInterpolation(matrix, f, lagrange0, coeffs0, lagrange1, coeffs1);

  std::cout << " test interpolation P" << order0 << " -> P" << order1 << " : error = " << diff << std::endl;

  return diff < 1e-12;
}


template<int order, class GW0, class GW1>
bool testRaviartThomas(const GW0& gw0, const GW1& gw1)
{
  std::cout << " test Raviart-Thomas bases " << GW0::dimension << "D" <<  std::endl;

  Functions::RaviartThomasBasis<GW0,order> coarseBasis(gw0);
  Functions::RaviartThomasBasis<GW1,order> fineBasis(gw1);

  // TODO Remove FieldMatrix/FieldVector here!
  BCRSMatrix<FieldMatrix<double,1,1>> matrix;
  BlockVector<FieldVector<double,1>> coarseCoeffs, fineCoeffs;

  // assemble the matrix
  assembleGlobalBasisTransferMatrix(matrix,coarseBasis,fineBasis);

  // construct some nontrivial function in the coarse-grid Raviart-Thomas space
  coarseCoeffs.resize(coarseBasis.dimension());
  for (std::size_t i=0; i<coarseCoeffs.size(); i++)
    coarseCoeffs[i] = i*std::sqrt(2.0);

  // use the transfer matrix to inject that function into the fine-grid space
  fineCoeffs.resize(fineBasis.dimension());
  matrix.mv(coarseCoeffs,fineCoeffs);

  // the two space are nested, hence both functions should be the same.
  // Let's check that!
  using Range = FieldVector<double,GW0::dimension>;
  auto coarseFunction = Functions::makeDiscreteGlobalBasisFunction<Range>(coarseBasis, coarseCoeffs);
  auto fineFunction   = Functions::makeDiscreteGlobalBasisFunction<Range>(  fineBasis,   fineCoeffs);

  auto coarseLocalFunction = localFunction(coarseFunction);
  auto fineLocalFunction   = localFunction(fineFunction);

  for (const auto& element : elements(gw1))
  {
    coarseLocalFunction.bind(element.father());
    fineLocalFunction.bind(element);

    FieldVector<double,2> testPoint(0);
    auto testPointInFather = element.geometryInFather().global(testPoint);

    if ( (coarseLocalFunction(testPointInFather) - fineLocalFunction(testPoint) ).two_norm() > 1e-10 )
    {
      std::cerr << "Raviart-Thomas: transfer matrix is not the natural injection!" << std::endl;
      return false;
    }

  }

  return true;
}


template<int order, class GW0, class GW1, class GW2>
bool testTwoLevelLagrange(const GW0& gw0, const GW1& gw1, const GW2& gw2)
{

  // create 3 global bases
  auto coarseLagrange = Functions::BasisFactory::makeBasis(gw0, Functions::BasisFactory::lagrange<order>());
  auto semiLagrange = Functions::BasisFactory::makeBasis(gw1, Functions::BasisFactory::lagrange<order>());
  auto fineLagrange = Functions::BasisFactory::makeBasis(gw2, Functions::BasisFactory::lagrange<order>());

  // matrices for the combined jump and two simple jumps
  BCRSMatrix<FieldMatrix<double,1,1>> matrixTwoJumps, matrixJumpOne, matrixJumpTwo;

  // assemble the matrices
  assembleGlobalBasisTransferMatrix(matrixTwoJumps,coarseLagrange,fineLagrange);
  assembleGlobalBasisTransferMatrix(matrixJumpOne,coarseLagrange,semiLagrange);
  assembleGlobalBasisTransferMatrix(matrixJumpTwo,semiLagrange,fineLagrange);

  // now check whether the product of two jumps is the combined jump
  // compute matrixTwoJumps -= matrixJumpTwo*matrixJumpOne
  for (auto row0 = matrixJumpTwo.begin(); row0 != matrixJumpTwo.end(); row0++)
    for (auto entry0 = row0->begin(); entry0 != row0->end(); entry0++)
      for (auto row1 = matrixJumpOne.begin(); row1 != matrixJumpOne.end(); row1++)
        for(auto entry1 = row1->begin(); entry1 != row1->end(); entry1++)
          if (row1.index() == entry0.index() )
            matrixTwoJumps[row0.index()][entry1.index()] -= (*entry0)*(*entry1);


  std::cout << " test 2-jump " << GW0::dimension << "D with order " << order  << " : error = " << matrixTwoJumps.frobenius_norm2()<< std::endl;

  return matrixTwoJumps.frobenius_norm2() < 1e-15;

}




// this tests the interpolation property of the matrix
template<class GW0, class GW1>
bool checkTransferMatrix(const GW0& gw0, const GW1& gw1)
{
  bool passed = true;

  // test Lagrange elements for orders 0-3
  // for scalar bases ...
  passed = passed and testLagrange<0>(gw0,gw1);
  passed = passed and testLagrange<1>(gw0,gw1);
  passed = passed and testLagrange<2>(gw0,gw1);
  // ... and powerBases
  passed = passed and testPowerBasis<0>(gw0,gw1);
  passed = passed and testPowerBasis<1>(gw0,gw1);
  passed = passed and testPowerBasis<2>(gw0,gw1);
  // order 3 only for 2D grids
  if constexpr ( GW0::dimension < 3 )
  {
    passed = passed and testLagrange<3>(gw0,gw1);
    passed = passed and testPowerBasis<3>(gw0,gw1);
  }

  // test transfer operators for Raviart-Thomas bases
  if constexpr ( GW0::dimension==2 )
    passed = passed and testRaviartThomas<0>(gw0,gw1);

    // 2D: check lagrange interpolation for different orders on the same grid
  if constexpr ( GW0::dimension < 3 )
  {
    passed = passed and testOrder<1,2>(gw1,gw1);
    passed = passed and testOrder<2,3>(gw1,gw1);
    passed = passed and testOrder<1,3>(gw1,gw1);
  }

  return passed;
}


// this test checks a jump over two levels
template<class GW0, class GW1, class GW2>
bool checkTwoLevelJump(const GW0& gw0, const GW1& gw1, const GW2& gw2)
{
  bool passed = true;

  // test order 0-3
  passed = passed and testTwoLevelLagrange<0>(gw0,gw1,gw2);
  passed = passed and testTwoLevelLagrange<1>(gw0,gw1,gw2);
  passed = passed and testTwoLevelLagrange<2>(gw0,gw1,gw2);
  // order 3 only for 2D grids
   if ( GW0::dimension < 3 )
    passed = passed and testTwoLevelLagrange<3>(gw0,gw1,gw2);

  return passed;
}


struct Suite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        auto maxLevel = grid.maxLevel();

        auto fineGridView = grid.levelGridView(maxLevel);
        auto semiGridView = grid.levelGridView(maxLevel-1);
        auto coarseGridView = grid.levelGridView(maxLevel-2);

        passed = passed and checkTransferMatrix(semiGridView,fineGridView);
        passed = passed and checkTwoLevelJump(coarseGridView,semiGridView,fineGridView);

        return passed;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    Suite tests;

    bool passed = checkWithStructuredGrid<2>(tests, 3);
    passed = passed and checkWithStructuredGrid<3>(tests, 3);

    return passed ? 0 : 1;
}
