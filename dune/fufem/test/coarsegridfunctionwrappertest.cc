#include <config.h>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/version.hh>

#include <dune/istl/bvector.hh>

#include <dune/fufem/functions/coarsegridfunctionwrapper.hh>

#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include "common.hh"



template <int dim, class RT>
class SinSin
{
    public:
        RT operator()(const Dune::FieldVector<double, dim>& x) const
        {
            double r=1.0;
            for(int i=0; i<dim; ++i)
                r*=sin(x[i]);
            return r;
        }
};



struct CoarseGridFunctionWrapperTestSuite
{
    template<class GridType>
    bool check(const GridType& grid)
    {
        typedef typename Dune::template FieldVector<double, 1> RangeType;

        const int dim = GridType::dimensionworld;

        typedef typename Dune::BlockVector<RangeType> Vector;

        typedef typename GridType::LevelGridView CoarseGridView;
        using CoarseBasis = Dune::Functions::LagrangeBasis<CoarseGridView, 1>;

        typedef typename GridType::LeafGridView FineGridView;
        using FineBasis = Dune::Functions::LagrangeBasis<FineGridView, 1>;


        CoarseBasis cBasis(grid.levelGridView(0));
        FineBasis fBasis(grid.leafGridView());


        SinSin<dim, RangeType> f;

        std::cout << "Interpolating analytic function on coarse grid." << std::endl;
        Vector cCoeff;
        Dune::Functions::interpolate(cBasis, cCoeff, f);
        auto cFunc = Dune::Functions::makeDiscreteGlobalBasisFunction<RangeType>(cBasis, cCoeff);

        CoarseGridFunctionWrapper<decltype(cFunc)> wrappedCFunc(cFunc);

        std::cout << "Interpolating wrapped coarse grid function on fine grid." << std::endl;
        Vector fCoeff;
        Dune::Functions::interpolate(fBasis, fCoeff, wrappedCFunc);
        auto fFunc = Dune::Functions::makeDiscreteGlobalBasisFunction<RangeType>(fBasis, fCoeff);


        bool passed = true;

        // The following tests need global evaluation of discrete functions.
        // This was implemented in the old functions interface based on virtual
        // inheritance (this is why these tests exist), but the modern
        // DiscreteGlobalBasisFunction implementation from dune-functions
        // only received this feature after dune-functions 2.9.
#if DUNE_VERSION_GT(DUNE_FUNCTIONS, 2, 9)
        std::cout << "Comparing coarse grid function and fine grid function by global evaluation." << std::endl;
        if (not(compareEvaluateByGridViewQuadrature(cFunc, fFunc, fBasis.gridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        std::cout << "Comparing wrapped coarse grid function and coarse grid function by global evaluation." << std::endl;
        if (not(compareEvaluateByGridViewQuadrature(wrappedCFunc, cFunc, fBasis.gridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        std::cout << "Comparing wrapped coarse grid function and fine grid function by global evaluation." << std::endl;
        if (not(compareEvaluateByGridViewQuadrature(wrappedCFunc, fFunc, fBasis.gridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }
#endif

        std::cout << "Comparing wrapped coarse grid function and fine grid function by local evaluation." << std::endl;
        if (not(compareEvaluateLocalByGridViewQuadrature(wrappedCFunc, fFunc, fBasis.gridView(), 2)))
        {
            std::cout << "Failed" << std::endl;
            passed = false;
        }

        return passed;
    }

};


int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    CoarseGridFunctionWrapperTestSuite tests;

    bool passed = checkWithStandardAdaptiveGrids(tests);

    return passed ? 0 : 1;
}
