#include <config.h>

#include <cmath>
#include <cstdio>
#include <algorithm>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/version.hh>
#include <dune/istl/bvector.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>

#if DUNE_VERSION_GTE(DUNE_FUNCTIONS, 2, 9)
#include <dune/functions/gridfunctions/composedgridfunction.hh>
#endif

#include <dune/fufem/assemblers/assembler.hh>
#include <dune/fufem/assemblers/dunefunctionsoperatorassembler.hh>
#include <dune/fufem/assemblers/localassemblers/laplaceassembler.hh>
#include <dune/fufem/assemblers/localassemblers/generalizedlaplaceassembler.hh>
#include <dune/fufem/backends/istlmatrixbackend.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

#include "common.hh"


/** \brief TestSuite for GeneralizedLaplaceAssembler
 *
 *  This TestSuite tests for consistency of the GeneralizedLaplaceAssembler with the LaplaceAssembler
 */
struct GeneralizedLaplaceAssemblerTestSuite
{
    template <typename GridType>
    bool check(const GridType& grid)
    {
        bool passed = true;

        std::cout << "Checking with scalar type FieldVector<double,1>" << std::endl;
        passed &= checkWithScalarType<GridType, typename Dune::FieldVector<double, 1> >(grid);

        std::cout << "Checking with scalar type FieldMatrix<double,1,1>" << std::endl;
        passed &= checkWithScalarType<GridType, typename Dune::FieldMatrix<double, 1, 1> >(grid);

        std::cout << "Checking with scalar type double" << std::endl;
        passed &= checkWithScalarType<GridType, double>(grid);

        return passed;
    }


    template<typename Range>
    std::vector<typename Range::size_type> orderedRangePattern(const Range& r)
    {
        std::vector<typename Range::size_type> pattern;
        auto it = r.begin();
        auto end = r.end();
        for(; it!=end; ++it)
            pattern.push_back(it.index());
        std::sort(pattern.begin(), pattern.end());
        return pattern;
    }

    template<typename MatrixType>
    bool checkMatrixEquality(const MatrixType& A, const MatrixType& B)
    {
        const double tol = 1e-15;
        if (not((A.N() == B.N()) and (A.M() == B.M())))
        {
            std::cout << "Matrices have different sizes" << std::endl;;
            return false;
        }
        for (size_t i=0; i<A.N(); ++i)
        {
            if (orderedRangePattern(A[i]) != orderedRangePattern(B[i]))
            {
                std::cout << "Matrices have different pattern in row " << i << std::endl;
                return false;
            }
            auto it = A[i].begin();
            auto end = A[i].end();
            for(; it!=end; ++it)
            {
                if (std::abs(*it - B[i][it.index()]) > tol)
                {
                    std::cout << "Matrices entries at (" << i << "," << it.index() << ") differ by more than " << tol << std::endl;
                    return false;
                }
            }
        }
        return true;
    }

    template <typename GridType, typename ScalarRangeType>
    bool checkWithScalarType(const GridType& grid)
    {
        bool result = true;

        const int dim = GridType::dimension;

        using Basis = Dune::Functions::LagrangeBasis<typename GridType::LeafGridView, 1>;
        using FufemBasis = DuneFunctionsBasis<Basis>;
        using namespace Dune::Functions::BasisBuilder;

        typedef typename FufemBasis::LocalFiniteElement FE;

        Basis basis(grid.leafGridView());
        FufemBasis fufemBasis(basis);

        typedef typename GridType::template Codim<0>::Geometry::GlobalCoordinate DomainType;
        typedef Dune::FieldMatrix<double, dim, dim> MatrixRangeType;

        Assembler<FufemBasis, FufemBasis> assembler(fufemBasis, fufemBasis);

        // assemble without coefficient (LaplaceAssembler) for comparison
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat;
        Dune::Fufem::DuneFunctionsOperatorAssembler<Basis, Basis> operatorAssembler(basis,basis);
        Dune::Fufem::LaplaceAssembler laplaceAssembler;
        operatorAssembler.assembleBulk(Dune::Fufem::istlMatrixBackend(stiff_mat), laplaceAssembler);

        // test with scalar coefficient functions
        using ConstantScalarFunction = std::function<ScalarRangeType(DomainType)>;

        // assemble and check with scalar coefficient 1
        auto scalarconstant1 = [](auto&&){ return 1.0;};
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_s1;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantScalarFunction> genLaplaceAssembler_s1(scalarconstant1,2);
        assembler.assembleOperator(genLaplaceAssembler_s1, stiff_mat_s1);
        result &= checkMatrixEquality(stiff_mat, stiff_mat_s1);

        // assemble and check with scalar coefficient 2
        auto scalarconstant2 = [](auto&&){ return 2.0;};
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_s2;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantScalarFunction> genLaplaceAssembler_s2(scalarconstant2,2);
        assembler.assembleOperator(genLaplaceAssembler_s2, stiff_mat_s2);
        stiff_mat_s2 *= 0.5;
        result &= checkMatrixEquality(stiff_mat, stiff_mat_s2);

        // assemble and check with scalar coefficient 3
        std::vector<double> coeff;
        Dune::Functions::interpolate(basis, coeff, [](auto&&){ return 3.0;});
        auto scalarconstant3 = Dune::Functions::makeDiscreteGlobalBasisFunction<ScalarRangeType>(basis, coeff);
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_s3;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantScalarFunction> genLaplaceAssembler_s3(scalarconstant3,2);
        assembler.assembleOperator(genLaplaceAssembler_s3, stiff_mat_s3);
        stiff_mat_s3 /= 3.0;
        result &= checkMatrixEquality(stiff_mat, stiff_mat_s3);

        // test with matrix-valued coefficient functions
        using ConstantMatrixFunction = std::function<MatrixRangeType(DomainType)>;

        MatrixRangeType Id(0.0);
        for (int d=0; d<dim; ++d)
            Id[d][d] = 1.0;

        // assemble and check with matrix coefficient 1*Id
        auto matrixconstant1 = [=](auto&&){ return Id;};
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_m1;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantMatrixFunction> genLaplaceAssembler_m1(matrixconstant1,2);
        assembler.assembleOperator(genLaplaceAssembler_m1, stiff_mat_m1);
        result &= checkMatrixEquality(stiff_mat, stiff_mat_m1);

        // assemble and check with matrix coefficient 2*Id
        auto matrixconstant2 = [=](auto&&){ return Id*2.0;};
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_m2;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantMatrixFunction> genLaplaceAssembler_m2(matrixconstant2,2);
        assembler.assembleOperator(genLaplaceAssembler_m2, stiff_mat_m2);
        stiff_mat_m2 *= 0.5;
        result &= checkMatrixEquality(stiff_mat, stiff_mat_m2);

#if DUNE_VERSION_GTE(DUNE_FUNCTIONS, 2, 9)
        // assemble and check with matrix coefficient 3*Id
        auto matrixconstant3 = Dune::Functions::makeComposedGridFunction([=](auto&& scalar) { return Id*double(scalar);}, scalarconstant3);
        Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> > stiff_mat_m3;
        GeneralizedLaplaceAssembler<GridType,FE,FE,ConstantMatrixFunction> genLaplaceAssembler_m3(matrixconstant3,2);
        assembler.assembleOperator(genLaplaceAssembler_m3, stiff_mat_m3);
        stiff_mat_m3 /= 3.0;
        result &= checkMatrixEquality(stiff_mat, stiff_mat_m3);
#endif

        return result;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    std::cout << "This is the GeneralizedLaplaceAssemblerTest" << std::endl;

    GeneralizedLaplaceAssemblerTestSuite tests;

    bool passed = true;

    passed = checkWithStandardAdaptiveGrids(tests);


    return passed ? 0 : 1;

}
