// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#include <config.h>

#include <iostream>

#include <dune/common/exceptions.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/functions/functionspacebases/lagrangebasis.hh>
#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/gridfunctions/discreteglobalbasisfunction.hh>
#include <dune/functions/gridfunctions/test/gridfunctiontest.hh>

#include <dune/fufem/functions/vintagebasisgridfunction.hh>
#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

using namespace Dune;
using namespace Dune::Functions;
using namespace Dune::Functions::Test;




int main (int argc, char* argv[]) try
{
  using namespace Dune;
  using namespace Dune::Functions;
  using namespace Dune::Functions::BasisFactory;

  Dune::MPIHelper::instance(argc, argv);
  bool passed = true;


  // Generate grid for testing
  const int dim = 2;
  
  using Grid = Dune::YaspGrid<dim>;

  auto l = Dune::FieldVector<double,dim>(1);
  auto elements = std::array<int,dim>{{10, 10}};
  auto grid = Grid(l,elements);
  grid.globalRefine(2);

  using GridView = Grid::LeafGridView;
  using Domain = GridView::template Codim<0>::Geometry::GlobalCoordinate;

  auto gridView = grid.leafGridView();



  {
    auto duneFunctionsBasis = makeBasis(gridView, lagrange<3>());
    auto duneFufemBasis = DuneFunctionsBasis<decltype(duneFunctionsBasis)>(duneFunctionsBasis);

    using Range = double;
    using Vector = std::vector<Range>;
    auto f = [](const Domain& x){
      return x[0];
    };
    double exactIntegral = 0.5;

    // Construct some coefficient vector by interpolation
    auto x = Vector{};
    Dune::Functions::interpolate(duneFunctionsBasis, x, f);

    // Create and use VintageBasisGridFunction
    auto gf = Dune::Fufem::makeVintageBasisGridFunction<Range>(duneFufemBasis, x);
    auto y = Vector{};
    Dune::Functions::interpolate(duneFunctionsBasis, y, gf);

    passed = passed and (x==y);

    passed = passed and Dune::Functions::Test::checkGridViewFunction(gridView, gf, exactIntegral);
  }

  return passed ? 0: 1;
} catch ( Dune::Exception &e )
{
  std::cerr << "Dune reported error: " << e << std::endl;
  return 1;
}
catch(...)
{
  std::cerr << "Unknown exception thrown!" << std::endl;
  return 1;
}
