#include <config.h>

#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bvector.hh>

#include <dune/functions/functionspacebases/interpolate.hh>
#include <dune/functions/functionspacebases/lagrangebasis.hh>

#include <dune/fufem/functiontools/gridfunctionadaptor.hh>
#include <dune/fufem/functionspacebases/conformingbasis.hh>

#include <dune/fufem/functionspacebases/dunefunctionsbasis.hh>

#include "common.hh"


template <int dim, class RT>
class SinSin
{
    public:
        RT operator()(const Dune::FieldVector<double, dim>& x) const
        {
            double r=1.0;
            for(int i=0; i<dim; ++i)
                r*=sin(x[i]);
            return r;
        }
};



template<class RT>
struct Suite
{
    template<class GridType>
    bool check(GridType& grid)
    {
        const int dim = GridType::dimensionworld;
        typedef typename GridType::LeafGridView View;
        using NonconformingBasis = Dune::Functions::LagrangeBasis<View, 1>;
        typedef ConformingBasis<DuneFunctionsBasis<NonconformingBasis> > Basis;

        typedef Dune::BlockVector<RT> Vector;

        NonconformingBasis nonconformingBasis(grid.leafGridView());
        DuneFunctionsBasis<NonconformingBasis> fufemNonconformingBasis(nonconformingBasis);
        Basis basis(fufemNonconformingBasis);

        Vector v;

        SinSin<dim, RT> f;
        Dune::Functions::interpolate(nonconformingBasis, v, f);

        GridFunctionAdaptor<Basis> adaptor(basis, true);

        refineLocally(grid, 1);

        nonconformingBasis.update(grid.leafGridView());
        basis.update(grid.leafGridView());

        adaptor.adapt(v);

        bool passed = (v.size() == basis.size());

        return passed;
    }

};

template<class RT>
bool checkForRangeType()
{
    Suite<RT> tests;
    return checkWithStandardAdaptiveGrids(tests);
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    bool passed = true;

    passed = passed and checkForRangeType<Dune::FieldVector<double, 1> >();
    passed = passed and checkForRangeType<Dune::FieldVector<int, 1> >();

    return passed ? 0 : 1;
}
