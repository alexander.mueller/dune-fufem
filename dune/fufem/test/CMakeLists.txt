# Path to the example grid files in dune-grid
add_definitions(-DDUNE_GRID_EXAMPLE_GRIDS_PATH=\"${DUNE_GRID_EXAMPLE_GRIDS_PATH}\")

# Tests that should be run unconditionally
dune_add_test(SOURCES assembletransferoperatortest.cc)
dune_add_test(SOURCES basisgridfunctiontest.cc)
dune_add_test(SOURCES boundarypatchprolongatortest.cc)
dune_add_test(SOURCES boundarypatchtest.cc)
dune_add_test(SOURCES coarsegridfunctionwrappertest.cc)
dune_add_test(SOURCES constructboundarydofstest.cc)
dune_add_test(SOURCES discretizationerrortest.cc)
dune_add_test(SOURCES dunefunctionsipdgassemblertest.cc)
dune_add_test(SOURCES functionintegratortest.cc)
dune_add_test(SOURCES functionspacebasistest.cc)
dune_add_test(SOURCES generalizedlaplaceassemblertest.cc)
dune_add_test(SOURCES gradientassemblertest.cc)
dune_add_test(SOURCES gridconstructiontest.cc)
dune_add_test(SOURCES gridfunctionadaptortest.cc)
dune_add_test(SOURCES gridfunctiontest.cc)
dune_add_test(SOURCES h1functionalassemblertest.cc)
dune_add_test(SOURCES integraloperatorassemblertest.cc)
dune_add_test(SOURCES istlbackendtest.cc)
dune_add_test(SOURCES laplaceassemblertest.cc)
dune_add_test(SOURCES localassemblertest.cc)
dune_add_test(SOURCES makerefinedsimplexgeometrytest.cc)
dune_add_test(SOURCES mappedmatrixtest.cc)
dune_add_test(SOURCES newpfeassemblertest.cc)
dune_add_test(SOURCES parallelassemblytest.cc)
dune_add_test(SOURCES pgmtest.cc)
dune_add_test(SOURCES ppmtest.cc)
dune_add_test(SOURCES refinedsimplexgeometrytest.cc)
dune_add_test(SOURCES secondorderassemblertest.cc)
dune_add_test(SOURCES subgridxyfunctionalassemblertest.cc)
dune_add_test(SOURCES symmetricmatrixtest.cc)
dune_add_test(SOURCES tensortest.cc)
dune_add_test(SOURCES test-polyhedral-minimisation.cc)
dune_add_test(SOURCES transferoperatorassemblertest.cc)
dune_add_test(SOURCES vintagebasisgridfunctiontest.cc)

# PYTHONLIBS_FOUND is just placed for backward compatibility with 2.7 Core tests
# and can be removed once tests against 2.7 Core are disabled
if (Python3_FOUND OR PYTHONLIBS_FOUND)
  dune_add_test(SOURCES dunepythontest.cc)

  # Add python flags to corresponding test
  add_dune_pythonlibs_flags(dunepythontest)

  # Copy python file to build directory
  dune_add_copy_dependency(dunepythontest "dunepythontest.py")
endif()

if (ADOLC_FOUND)
    dune_add_test(SOURCES adolctest.cc)
    add_dune_adolc_flags(adolctest)

    dune_add_test(SOURCES adolcfunctiontest.cc)
    add_dune_adolc_flags(adolcfunctiontest)
endif()

if(Boost_SERIALIZATION_FOUND)
  dune_add_test(SOURCES serializationtest.cc)
  set_property(TARGET serializationtest APPEND PROPERTY INCLUDE_DIRECTORIES ${Boost_INCLUDE_DIRS})
  target_link_libraries(serializationtest PUBLIC ${Boost_SERIALIZATION_LIBRARY})
endif()

if (HDF5_FOUND)
  dune_add_test(SOURCES test-hdf5.cc)
  add_dune_hdf5_flags(test-hdf5)
endif()
